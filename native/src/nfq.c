/*
 * NFQ - Heimdall NFQ interface for Linux
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE
#include <errno.h>
#include <arpa/inet.h>
#include "huvud.h"
#include "l3.h"

#define MAX_PACKET_SIZE 16384

static const uint32_t NO_MARK        = 0;
static const uint32_t ACCEPT_MARK    = 0xb1f40570;
static const uint32_t INTERCEPT_MARK = 0xb1f40571;
static const uint32_t DROP_MARK      = 0xb1f40572;
static const uint32_t REJECT_MARK    = 0xb1f40573;

static const char* verdict2str(uint32_t verdict) {
  switch (verdict) {
      case NF_ACCEPT: return "NF_ACCEPT";
      case NF_DROP:   return "NF_DROP";
    }
  return "<UNKNOWN>";
}

void set_verdict(huvud_queue_t* hq_p, uint32_t pkt_id, uint32_t verdict, uint32_t mark, void* data,
                 uint32_t len) {
  int rc = nfq_set_verdict2(hq_p->hw.qh, pkt_id, verdict, mark, len, data);
#ifdef __DEBUG__
  LOG(hq_p, DEBUG, "Issued %s verdict on pkt %u (%u bytes @0x%p) mark 0x%x",
      verdict2str(verdict), pkt_id, len, data, mark);
#endif
}

void accept_packet(huvud_queue_t* hq_p, uint32_t pkt_id, void* data, uint32_t len) {
  set_verdict(hq_p, pkt_id, NF_ACCEPT, NO_MARK, data, len);
}

void steal_packet(huvud_queue_t* hq_p, uint32_t pkt_id, void* data, uint32_t len) {
  set_verdict(hq_p, pkt_id, NF_STOLEN, NO_MARK, data, len);
}

void drop_packet(huvud_queue_t* hq_p, uint32_t pkt_id, void* data, uint32_t len) {
  set_verdict(hq_p, pkt_id, NF_DROP, NO_MARK, data, len);
}

int handle_packet(huvud_queue_t* hq_p, uint32_t id, uint8_t* data, uint32_t len) {

}

static int handle_nfq_message(struct nfq_q_handle *qh,
                              struct nfgenmsg *nfmsg,
                              struct nfq_data *nfa,
                              void* opaque) {
  uint8_t* data;
  uint32_t len = nfq_get_payload(nfa, &data);

  struct nfqnl_msg_packet_hdr* hdr = nfq_get_msg_packet_hdr(nfa);

  huvud_queue_t* hq_p = opaque;
  uint32_t id         = ntohl(hdr->packet_id);

  switch (handle_l3_packet(hq_p, id, data, len)) {
  case ACCEPT_PACKET:
    accept_packet(hq_p, id, data, len);
    break;
  case STEAL_PACKET:
    steal_packet(hq_p, id, data, len);
    break;
  case DROP_PACKET:
    drop_packet(hq_p, id, data, len);
    break;
  case ABORT_ALL_PROCESSING:
    return -1;
  default:
    break;
  }
  return 0;
}

int hw_init(huvud_queue_t* hq_p, const char* device_unused, const char* filter_exp_unused) {
  int rc = OK;

  if (!hq_p) {
    LOG(hq_p, ERROR, "Unable to initialize invalid queue");
    rc = EFAULT;
    goto out;
  }

  hq_p->hw.h = nfq_open();

  if (!hq_p->hw.h) {
    LOG(hq_p, ERROR, "nfq_open() failure");
    rc = EIO;
    goto out;
  }

  nfq_unbind_pf(hq_p->hw.h, AF_INET);

  nfq_unbind_pf(hq_p->hw.h, AF_INET6);

  if (nfq_bind_pf(hq_p->hw.h, AF_INET) < 0) {
    LOG(hq_p, ERROR, "nfq_bind(AF_INET) failure");
    rc = EINVAL;
    goto err1;
  }
  if (nfq_bind_pf(hq_p->hw.h, AF_INET6) < 0) {
    LOG(hq_p, ERROR, " nfq_bind(AF_INET6) failure");
    rc = EINVAL;
    goto err1;
  }

  hq_p->hw.qh = nfq_create_queue(hq_p->hw.h, hq_p->id, &handle_nfq_message, hq_p);

  if (!hq_p->hw.qh) {
    LOG(hq_p, ERROR, "nfq_create_queue(%u) failed\n", hq_p->id);
    rc = EBADF;
    goto err1;
  }

  if (nfq_set_mode(hq_p->hw.qh, NFQNL_COPY_PACKET, 0xFFFF) < 0) {
    LOG(hq_p, ERROR, "nfq_set_mode() failed\n");
    rc = ENOTTY;
    goto err2;
  }

  LOG(hq_p, INFO, "Created NFQ %u\n", hq_p->id);

 out:
  return rc;

 err2:
  nfq_destroy_queue(hq_p->hw.qh);
 err1:
  nfq_close(hq_p->hw.h);
  goto out;
}

void hw_destroy(huvud_queue_t* hq_p) {
  if (hq_p != NULL) {
    if (hq_p->hw.qh != NULL) {
      nfq_destroy_queue(hq_p->hw.qh);
    }
    if (hq_p->hw.h) {
      nfq_close(hq_p->hw.h);
    }
    LOG(hq_p, INFO, "Destroyed NFQ %u\n", hq_p->id);
  }
}

void hw_run_queue(huvud_queue_t* hq_p) {
  int fd    = nfq_fd(hq_p->hw.h);
  void* buf = malloc(MAX_PACKET_SIZE);

  for (;;) {
    ssize_t read_bytes = recv(fd, buf, MAX_PACKET_SIZE, 0);

#ifdef __DEBUG__
    LOG(hq_p, DEBUG, "Received %lu bytes", read_bytes);
#endif

    if (read_bytes < 0) {
      LOG(hq_p, INFO, "Aborting NFQ servicing of queue #%d", hq_p->id);
      break;
    }
    if (nfq_handle_packet(hq_p->hw.h, buf, read_bytes) != 0 || errno != 0) {
      LOG(hq_p, INFO, "Canceling NFQ servicing of queue #%d: %s(%d)",
          hq_p->id, strerror(errno), errno);
      break;
    }
  }
  free(buf);
}
