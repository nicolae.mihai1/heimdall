/*
 * Huvud - Heimdall Universal Virtual Uniform Data-Interface
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __HUVUD_H__
#define __HUVUD_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcap.h>

#include "Heimdall/Interface/Queues_stub.h"

#ifdef LINUX
#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>

typedef struct {
  struct nfq_handle*   h;
  struct nfq_q_handle* qh;
} platform_t;

#else
typedef struct {
  pcap_t*            session;
  struct bpf_program fp;
  int                link_type;
} platform_t;
#endif

/* For the love of Bob, do not touch this under penalty of a slow gruesome and painful
 * death!! This has to match the implementation of the Storable Packet instance in Heimdall.hs
 * There are ways to make this safer but it would be slower, so
 * in the interest of warp speed, there is a lot of manual peek pokery there. So just.. don't.
 *   _                   _
 * _( )                 ( )_
 * (_, |      __ __      | ,_)
 *    \'\    /  ^  \    /'/
 *     '\'\,/\      \,/'/'
 *       '\| []   [] |/'
 *         (_  /^\  _)
 *           \  ~  /
 *           /HHHHH\
 *         /'/{^^^}\'\
 *     _,/'/'  ^^^  '\'\,_
 *    (_, |           | ,_)
 *      (_)           (_)
 */
typedef struct {
  uint32_t id;
  uint32_t src_addr[4];  /* an IPv6 address is 4 32-bit words long */
  uint32_t dst_addr[4];
  uint8_t  addr_len;
  uint8_t  family;
  uint8_t  protocol;
  uint8_t  unused;
  uint16_t src_port;
  uint16_t dst_port;
  uint32_t total_payload;
  uint32_t available_payload;
  uint32_t pkt_len;
  uint8_t* pkt_start;
  uint8_t* payload;
} packet_t;

typedef enum {
  ACCEPT_PACKET = 1,
  DROP_PACKET,
  STEAL_PACKET,
  WORKING_ON_IT,
  ABORT_ALL_PROCESSING
} verdict_t;

struct huvud_queue_;

typedef void  (logfn_t)     (HsWord8 level, HsPtr component, HsPtr str);
typedef HsInt (analyzefn_t) (HsPtr q_p, HsPtr pkt_p);

#define MAX_COMPONENT_LENGTH 64

typedef struct huvud_queue_ {
  uint16_t       id;
  platform_t     hw;
  logfn_t*       log;
  analyzefn_t*   analyze;
  HsPtr          q_p;
  char           component[MAX_COMPONENT_LENGTH];
} huvud_queue_t;

#define LOG2(_FUN, _LEVEL, _COMP, ...) { \
    char* __buf_p;                              \
    int _ = asprintf(&__buf_p, __VA_ARGS__);    \
    _FUN(_LEVEL, _COMP, __buf_p);               \
    free(__buf_p);                              \
  }

static const uint32_t DEBUG     = 0;
static const uint32_t INFO      = 1;
static const uint32_t NOTICE    = 2;
static const uint32_t WARNING   = 3;
static const uint32_t ERROR     = 4;
static const uint32_t CRITICAL  = 5;
static const uint32_t EMERGENCY = 6;

#define OK 0

#define LOG(_Q, _LEVEL, ...) LOG2(_Q->log, _LEVEL, _Q->component, __VA_ARGS__)

/* HW API */
int  hw_init(huvud_queue_t* hq_p, const char* device, const char* filter_exp);
void hw_destroy(huvud_queue_t* hq_p);
void hw_run_queue(huvud_queue_t* hq_p);

/* API */

huvud_queue_t*
huvud_init (uint16_t q_id,
            char* component,
            const char* device,
            const char* filter_exp,
            logfn_t* log_fn);

void
huvud_run_queue(huvud_queue_t* hq_p);

void
huvud_destroy (huvud_queue_t* hq_p);

void
hexdump(char* buf, uint32_t bufLen, const void* data, uint32_t len);

#define SNPRINTF(_BUF, _LEN, ...) \
  { int _w = snprintf(_BUF, _LEN, __VA_ARGS__);\
    if (_w > _LEN) { _LEN = 0; _BUF = 0;} else { _LEN -= _w; _BUF += _w;}\
  }

#define min(_A, _B) (_A > _B ? _B : _A)

#endif
