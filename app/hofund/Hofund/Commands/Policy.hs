{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.Policy where

import Control.Monad.Except         (runExceptT)
import Control.Monad.State.Strict   (gets)
import Data.ByteString.Char8        (pack)
import Data.Configurable            (serialize)
import Data.Monoid                  ((<>))
import Data.Referable               (deadRef)
import Database.Adapter             (del, delMapKVs, setMapKVs)
import Heimdall.Types
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Action, Commands)

import qualified System.Console.StructuredCLI     as CLI

policy :: Commands
policy =
  custom "policy" "Configure a traffic policy" parsePolicyName always setTarget >+ do
    basic
    command "add" "Add entry to a routing policy" newLevel >+ do
           addRule
           basic
    command "remove" "Remove entry from a routing policy" newLevel >+ do
           basic
           removeRule
    showPolicy
    destroy

addRule :: Commands
addRule =
  param "rule" "<rule priority>" asInt setArg1 >+ do
    basic
    continue'
    discard
    routeVia
    loadbalance

removeRule :: Commands
removeRule =
  param "rule" "<rule priority>" asInt $ \priority -> do
    name <- getTarget
    db   <- gets db
    str  <- serialize priority
    let key = policyKeyPrefix <> name
    delMapKVs db key [str]
    return NoAction

routeViaCmd :: Handler StateM IntfName -> Commands
routeViaCmd = custom "route-via" "<interface name to route traffic out on>" parseIntf always

routeVia :: Commands
routeVia = routeViaCmd $ setRuleAction . RouteVia . deadRef

loadbalanceCmd :: Handler StateM String -> Commands
loadbalanceCmd = custom "loadbalance" hint (parseNameFor routeMapKeyPrefix hint) always
                    where hint = "Perform traffic loadbalancing over a route-map"

loadbalance :: Commands
loadbalance = loadbalanceCmd $ setRuleAction . Loadbalance . deadRef . pack

discard :: Commands
discard = command "discard" "Drop traffic" $ setRuleAction Discard

continue' :: Commands
continue' = command "continue" "Drop traffic" $ setRuleAction Continue

setRuleAction :: Action -> StateM CLI.Action
setRuleAction action = do
    policyName  <- getTarget
    priority    <- getArg1
    let key = policyKeyPrefix <> policyName
    db <- gets db
    let save x = setMapKVs db key [(priority, x)]
    str <- runExceptT $ serialize action
    either (error $ "failure to serialize action " ++ show action) save str
    return NoAction

showPolicies :: Commands
showPolicies =
    command "policies" "Show configured traffic policies" $ do
      showObjects policyKeyPrefix
      return NoAction

showPolicy ::  Commands
showPolicy =
    command "show" "Display traffic policy contents" $ do
      policyName <- getTarget
      showObject (policyKeyPrefix <> policyName) Nothing
      return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this policy from the system" $ do
    db <- gets db
    policyName <- getTarget
    del db [policyKeyPrefix <> policyName]
    return NoAction
