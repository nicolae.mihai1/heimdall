{- |
Description: Hofund - Heimdall CLI - common commands
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Hofund.Commands.Logging where

import Control.Monad.IO.Class        (liftIO)
import Control.Monad.State.Strict    (gets)
import Data.ByteString.Char8         (ByteString, unpack)
import Data.Configurable             (serialize)
import Database.Adapter.Redis        (Redis)
import Database.Adapter              (Adapter(..))
import Hofund.Commands.Common
import Hofund.Types
import System.Console.StructuredCLI hiding (Commands)
import System.Logging

logging :: Commands
logging =
  command "logging" "Logger configuration" newLevel >+ do
    basic
    showLevels
    enableLevel
    disableLevel
    resetLevels

showLevels :: Commands
showLevels =
  command "show" "List enabled logger priorities" $ do
    db      <- gets db
    results <- getSet db levelsKey
    printList results
    return NoAction
        where printList = printOutput . mapM_ (putStrLn . unpack)

enableLevel :: Commands
enableLevel = setLoggingLevel "enable" addToSet

disableLevel :: Commands
disableLevel = setLoggingLevel "disable" delFromSet

resetLevels :: Commands
resetLevels =
  command "reset" "clear all previously enabled logger priorities" $ do
    db <- gets db
    del db [levelsKey]
    return NoAction

setLoggingLevel :: String ->
                   (Redis -> ByteString -> [ByteString] -> IO ()) ->
                   Commands
setLoggingLevel name op =
  param name "<Logging priority ([component]:<DEBUG | INFO | NOTICE | WARNING | ERROR | CRITICAL | EMERGENCY>) >" parseLevel $ \level -> do
    db  <- gets db
    str <- serialize level
    liftIO $ op db levelsKey [str]
    return NoAction

parseLevel :: Validator StateM Level
parseLevel = asValue
