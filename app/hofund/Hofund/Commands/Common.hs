{-# LANGUAGE OverloadedStrings, RankNTypes #-}
{- |
Description: Hofund - Heimdall CLI - common commands
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Hofund.Commands.Common where

import Control.Monad                 (void)
import Control.Monad.Except          (ExceptT, runExceptT, throwError)
import Control.Monad.Failable        (failableIO)
import Control.Monad.IO.Class        (MonadIO, liftIO)
import Control.Monad.State.Strict    (execStateT, get, gets, modify)
import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Maybe     (runMaybeT)
import Data.Bifunctor                (first)
import Data.ByteString.Char8         (ByteString, append, pack, stripPrefix, unpack)
import Data.Char                     (isSpace)
import Data.Configurable             (Configurable(..))
import Data.Default                  (Default, def)
import Data.IP                       (IP, IPv4, IPv6, AddrRange)
import Data.List                     (isPrefixOf)
import Data.Maybe                    (catMaybes, fromJust)
import Data.Monoid                   ((<>))
import Database.Adapter              (getMap, searchAll, setMapKVs)
import Heimdall.Common.ACL           (Help, ParserT)
import Heimdall.Common.ObjectModel
import Heimdall.Types         hiding (Partial)
import Hofund.Types
import Network.Socket                (SockAddr)
import System.Console.StructuredCLI  hiding (Action, Commands)
import Text.Read                     (readMaybe)

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified System.Console.StructuredCLI as CLI

basic :: CommandsT StateM ()
basic = do
  command "top" "return to the top of the tree" top
  command "exit" "go back one level up" exit

cmdFailure :: (Show e, MonadIO m) => String -> e -> m ()
cmdFailure str err =
  liftIO . putStrLn $ "Failure " ++ str ++ ": " ++ show err

intfTypes :: [ByteString]
intfTypes = pack.show <$> ([minBound..maxBound] :: [IntfType])

asValue :: (MonadIO m, Configurable a) => String -> m (Maybe a)
asValue = runMaybeT . deserialize . pack

asInt :: (MonadIO m) => String -> m (Maybe Int)
asInt = asValue

asString :: (MonadIO m) => String -> m (Maybe String)
asString = return . Just

asVRFID :: (MonadIO m) => String -> m (Maybe VRFID)
asVRFID = asValue

asIP ::  (MonadIO m) => String -> m (Maybe IP)
asIP = asValue

asV6CIDR :: (MonadIO m) => String -> m (Maybe (AddrRange IPv6))
asV6CIDR = asValue

asV4CIDR :: (MonadIO m) => String -> m (Maybe (AddrRange IPv4))
asV4CIDR = asValue

asSockAddr :: (MonadIO m) => String -> m (Maybe SockAddr)
asSockAddr = asValue

parseIP :: (MonadIO m) => Parser m IP
parseIP = paramParser "<IP address>" asIP

parseSockAddr :: (MonadIO m) => Parser m SockAddr
parseSockAddr = paramParser "<network address (IPv4:port | [IPv6]:port)>" asSockAddr

parseName :: (MonadIO m) => String -> Parser m String
parseName hint = paramParser hint asString

parseVRFID :: (MonadIO m) => Parser m VRFID
parseVRFID = paramParser "<VRF ID>" asVRFID

parseConnTag :: (MonadIO m) => Parser m ConnectionTag
parseConnTag = paramParser "<Connection tag>" $ asValue

parseNameFor :: ByteString -> String -> Parser StateM String
parseNameFor prefix hint node str =
  labelParser node str >>= parseName'
      where parseName' (Done _ matched rest) = do
                       let (word, remaining) = break isSpace $ dropWhile isSpace rest
                       case word of
                         "" -> do
                           completions <- buildCompletions
                           return $ Partial completions ""
                         "?" ->
                           return $ Fail hint rest
                         _  ->
                           return $ Done word (matched ++ ' ':word) remaining
            parseName' result = return result
            buildCompletions = do
              names <- getAllNames prefix
              return $ zip names $ repeat ""

parsePolicyName :: Parser StateM String
parsePolicyName = parseNameFor policyKeyPrefix hint
        where hint = "<policy name to configure>"

parseIntf :: Parser StateM IntfName
parseIntf node str =
  labelParser node str >>= parseIntf'
      where parseIntf' (Done _ _ rest) = do
                       let result = flip A.parse (pack rest) $ do
                                           A.skipSpace
                                           parseIntfName
                       case result of
                         A.Partial continuation ->
                             process $ continuation ""
                         _ ->
                             process result
            parseIntf' (Fail hint' rest)          = return $ Fail hint' rest
            parseIntf' (Partial completions rest) = return $ Partial completions rest
            parseIntf' NoMatch                    = return NoMatch
            process (A.Fail "" _ _)    = do
                       completions <- buildCompletions intfTypePrefixes
                       return $ Partial completions ""
            process (A.Done rest intf) = return $ Done intf (show intf) (unpack rest)
            process (A.Fail i _ _)     = do
                let rest = unpack i
                case filter (isPrefixOf rest) intfTypePrefixes of
                  [] ->
                      case rest of
                        '?':_ ->
                            return . Fail hint $ rest
                        _ ->
                            return . Fail ("Invalid interface name: " ++ show rest) $ rest
                  matches -> do
                       completions <- buildCompletions matches
                       return $ Partial completions ""
            process (A.Partial _)      = error $ "Unexpected partial result parsing: " ++ str
            hint = "<interface name (queue/<num> | tunnel/<num> | vrf/<num>)"
            addSlash = (++ "/"). unpack
            intfTypePrefixes = addSlash <$> intfTypes
            buildCompletions matches = do
              allIntfs <- fmap show <$> getAllIntfs
              let intfNames = filter (\intf -> any (flip isPrefixOf intf) matches) allIntfs
              return $ zip (intfNames ++ matches) $ repeat ""

parseWith :: Parser StateM a -> Parser StateM a
parseWith parser node input =
  labelParser node input >>= handleResult
      where handleResult (Done _ _ remaining) =
                parser node remaining
            handleResult (Fail hint rest) =
                return $ Fail hint rest
            handleResult (Partial completions rest) =
                return $ Partial completions rest
            handleResult NoMatch =
                return NoMatch

parseRule :: (Default a) => ParserT a () -> Parser StateM (Int, a)
parseRule parser _ input = do
    result <- parse input
    process result
      where parse   = return . A.parse (runExceptT parser') . pack
            parser' = do
              priority <- parsePriority
              ace      <- execStateT parser def
              return $ (priority, ace)
            process (A.Partial continuation) =
                process $ continuation ""
            process (A.Done rest (Left hints)) =
                return . Partial hints $ unpack rest
            process (A.Done _ (Right str)) =
                return $ Done str input ""
            process (A.Fail remaining _ msg) = do
              result <- parse $ input ++ " ?"
              case result of
                (A.Done _ (Left hints)) ->
                    return $ Partial hints input
                _ ->
                  return $ Fail msg $ unpack remaining

parsePriority :: ExceptT Help A.Parser Int
parsePriority = do
  lift A.skipSpace
  help [("", "<integer priority>")]
  priority <- lift A.decimal
  lift . void $ A.space
  return priority
      where help hints = do
                c <- lift $ A.skipSpace >> A.peekChar
                case c of
                  Just '?' ->
                      throwError hints
                  _ ->
                      return ()

getAllIntfs :: StateM [IntfName]
getAllIntfs = do
  db <- gets db
  catMaybes . getIntfNames . concat <$> mapM (searchAll db) intfKeys
      where intfKeys = (`append` ".*") . append intfKeyPrefix <$> intfTypes
            getIntfNames = fmap getIntfName
            getIntfName  = fromParts . readType . readID . getIntfParts
            getIntfParts = span (/= '.') . unpack . fromJust . stripPrefix intfKeyPrefix
            fromParts (Just name, Just iD) = Just $ IntfName (name, iD)
            fromParts _ = Nothing
            readID      = fmap $ readMaybe . drop 1
            readType    = first readMaybe

setTarget :: (Configurable a) => a -> StateM CLI.Action
setTarget val = do
  str <- serialize val
  modify $ \AppState{..} -> AppState { target = str, .. }
  return NewLevel

getTarget :: StateM ByteString
getTarget = do
 AppState{..} <- get
 return target

setArg1 :: (Configurable a) => a -> StateM CLI.Action
setArg1 val = do
  str <- serialize val
  modify $ \AppState{..} -> AppState { arg1 = str, .. }
  return NewLevel

setArg2 :: (Configurable a) => a -> StateM CLI.Action
setArg2 val = do
  str <- serialize val
  modify $ \AppState{..} -> AppState { arg2 = str, .. }
  return NewLevel

getArg1 :: StateM ByteString
getArg1 = do
  AppState{..} <- get
  return arg1

getArg2 :: StateM ByteString
getArg2 = do
  AppState{..} <- get
  return arg2

printOutput :: (MonadIO m) => IO () -> m ()
printOutput actions = liftIO $ do
    putStrLn "==============================================================================="
    actions
    putStrLn ""

getAllNames :: ByteString -> StateM [String]
getAllNames prefix = do
  db <- gets db
  let wildcard = prefix <> "*"
  fmap (unpack.getNameFromKey) <$> searchAll db wildcard

showObjects :: ByteString -> StateM ()
showObjects prefix = do
  names <- getAllNames prefix
  printOutput $ mapM_ putStrLn names


showObject :: ByteString -> Maybe (IO ()) -> StateM ()
showObject key extra = do
  db   <- gets db
  info <- getMap db key
  printOutput $ mapM_ printProperty info >> perhaps extra
      where printProperty (name, value) =
                putStrLn $ unpack name ++ ": " ++ unpack value
            perhaps Nothing        = return ()
            perhaps (Just actions) = actions

setProperty :: (Configurable a) => ByteString -> ByteString -> a -> StateM CLI.Action
setProperty key property value = do
  db <- gets db
  status' <- runExceptT $ do
               str <- serialize value
               failableIO $ setMapKVs db key [(property, str)]
  either (cmdFailure $ "setting " ++ unpack key ++ ' ':unpack property) return status'
  return NoAction

always :: StateM Bool
always = return True
