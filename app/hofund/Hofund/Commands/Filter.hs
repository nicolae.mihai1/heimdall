{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.Filter where

import Control.Monad.State.Strict   (gets)
import Data.ByteString.Char8        (pack, unpack)
import Data.Configurable            (Serializations, serializations)
import Data.Monoid                  ((<>))
import Data.Referable               (deadRef)
import Database.Adapter             (del, delMapKVs)
import Heimdall.Types
import Hofund.Types
import Hofund.Commands.Common
import Hofund.Commands.Policy       (loadbalanceCmd)
import System.Console.StructuredCLI hiding (Action, Commands)


filterCmd :: Commands
filterCmd =
  custom "filter" hint (parseNameFor filterKeyPrefix hint) always setTarget >+ do
    basic
    command "set" "Set a given property for this filter" newLevel >+ do
      basic
      setDelegate
      setPolicy
    command "delete" "Delete a configuration property from this filter" newLevel >+ do
      basic
      delDelegate
      delPolicy
    depth
    defAction
    showFilter
    destroy
        where hint = "<name of filter to configure>"

showFilter :: Commands
showFilter =
    command "show" "Display filter configuration" $ do
      name <- getTarget
      showObject (filterKeyPrefix <> name) Nothing
      return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this filter from the system" $ do
    db   <- gets db
    name <- getTarget
    del db [filterKeyPrefix <> name]
    return NoAction

depth :: Commands
depth =
  custom "depth" "Set traffic analysis depth (application | in-depth | dpi)" parseDepth always $
    \filterDepth -> do
      name <- getTarget
      setProperty (filterKeyPrefix <> name) "depth" filterDepth
          where parseDepth node input = do
                  keywords::Serializations FilterDepth <- serializations
                  let depths = unpack . snd <$> keywords
                  parseOneOf depths "<depth for traffic analysis>" node input

defAction :: Commands
defAction =
    command "default-action" "Set action to perform if filter doesn't match" newLevel >+ do
      loadbalanceCmd $ \routeMapName -> do
        name <- getTarget
        let action = Loadbalance . deadRef . pack $ routeMapName
        setProperty (filterKeyPrefix <> name) defaultAction action
            where defaultAction = "default-action"

setDelegate :: Commands
setDelegate =
    custom "delegate" "Configure delegate server address" parseSockAddr always $ \addr -> do
      name <- getTarget
      setProperty (filterKeyPrefix <> name) "delegate" addr

delDelegate :: Commands
delDelegate =
    command "delegate" "Remove delegate server address" $ do
      db <- gets db
      name <- getTarget
      delMapKVs db (filterKeyPrefix <> name) ["delegate"]
      return NoAction

setPolicy :: Commands
setPolicy =
    custom "policy" "Set policy to use in case of a match" parsePolicyName always $ \name -> do
      filterName' <- getTarget
      setProperty (filterKeyPrefix <> filterName') "policy" name

delPolicy :: Commands
delPolicy =
    command "policy" "Remove filter traffic policy" $ do
      db   <- gets db
      name <- getTarget
      delMapKVs db (filterKeyPrefix <> name) ["policy"]
      return NoAction

showFilters :: Commands
showFilters =
    command "filters" "Show configured traffic filters" $ do
      showObjects filterKeyPrefix
      return NoAction
