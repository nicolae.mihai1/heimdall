{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.RouteMap where

import Control.Monad.State.Strict   (gets)
import Control.Monad.Except         (runExceptT)
import Data.ByteString.Char8        (ByteString, pack, unpack)
import Data.Configurable            (deserialize, serialize)
import Data.Monoid                  ((<>))
import Data.Set                     (Set)
import Database.Adapter
import Heimdall.Types
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Commands)

import qualified System.Console.StructuredCLI as CLI
import qualified Data.Set as Set

routeMap :: Commands
routeMap =
  custom "route-map" hint (parseNameFor routeMapKeyPrefix hint) always setTarget >+ do
        basic
        destroy
        command "add" "Add an entry to a given route map" newLevel >+ do
             basic
             addV4Route
             addV6Route
        command "remove" "Remove an entry from a route map" newLevel >+ do
             basic
             delV4Route
             delV6Route
        showRouteMap
            where hint = "<route map name to configure>"

addV4Route :: Commands
addV4Route =
  command "ipv4-route" "IPv4 route configuration" newLevel >+ do
    param "vrf" "VRF ID" asInt setArg1 >+ do
      param "address" "<IPv4 CIDR address>" asV4CIDR setArg2 >+ do
        basic
        addIntf

delV4Route :: Commands
delV4Route =
  command "ipv4-route" "IPv4 route configuration" newLevel >+ do
    param "vrf" "VRF ID" asInt setArg1 >+ do
      param "address" "<IPv4 CIDR address>" asV4CIDR setArg2 >+ do
        basic
        delIntf

addV6Route :: Commands
addV6Route =
  command "ipv6-route" "IPv6 route configuration" newLevel >+ do
    param "vrf" "VRF ID" asInt setArg1 >+ do
      param "address" "<IPv6 CIDR address>" asV6CIDR setArg2 >+ do
        basic
        addIntf

delV6Route :: Commands
delV6Route =
  command "ipv6-route" "IPv6 route configuration" newLevel >+ do
    param "vrf" "VRF ID" asInt setArg1 >+ do
      param "address" "<IPv6 CIDR address>" asV6CIDR setArg2 >+ do
        basic
        delIntf

addIntf :: Commands
addIntf =
  custom "next-hop-via" "<interface to add as next hop>" parseIntf always $ \intf -> do
    let intfStr = pack $ show intf
    routeMapOp $ Set.insert intfStr

routeMapOp :: (Set ByteString -> Set ByteString) -> StateM CLI.Action
routeMapOp operationOn = do
  db       <- gets db
  name     <- getTarget
  vrfID    <- getArg1
  prefix   <- getArg2
  let routeMapName = routeMapKeyPrefix <> name
  intfsStr <- perhapsEmpty <$> (runExceptT $ getMapKV db routeMapName prefix)
  result   <- runExceptT $ do
               intfs  <- Set.toList . operationOn . Set.fromList <$> deserialize intfsStr
               intfs' <- serialize intfs
               setMapKVs db routeMapName [(vrfID <> " => " <> prefix, intfs')]
  either (cmdFailure $ "Operation on route map " ++ unpack name ++ " failed: ") return result
  return NoAction
      where perhapsEmpty (Left _)    = ""
            perhapsEmpty (Right str) = str

delIntf :: Commands
delIntf =
  custom "next-hop-via" "<next hop interface to remove from route map>" parseIntf always $
    \intf -> do
      let intfStr = pack $ show intf
      routeMapOp $ Set.delete intfStr

showRouteMap :: Commands
showRouteMap =
  command "show" "Display route map contents" $ do
    name <- getTarget
    showObject (routeMapKeyPrefix <> name) Nothing
    return NoAction

showRouteMaps :: Commands
showRouteMaps =
  command "route-maps" "Show configured route maps" $ do
    showObjects routeMapKeyPrefix
    return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this policy from the system" $ do
    policyName <- getTarget
    db         <- gets db
    del db [routeMapKeyPrefix <> policyName]
    return NoAction
