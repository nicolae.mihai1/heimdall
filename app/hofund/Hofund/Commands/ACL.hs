{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.ACL where

import Control.Monad.State.Strict   (gets)
import Data.ByteString.Char8        (pack)
import Data.Configurable            (serialize)
import Data.Monoid                  ((<>))
import Database.Adapter             (del, delMapKVs, setMapKVs)
import Heimdall.Common.ACL          (aceParser)
import Heimdall.Types
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Commands)

acl :: Commands
acl =
  custom "acl" hint (parseNameFor aclKeyPrefix hint) always setTarget >+ do
        basic
        destroy
        command "add" "Add an entry to this ACL" newLevel >+ do
          basic
          addRule
        command "remove" "Remove an entry from this ACL" newLevel >+ do
          basic
          delRule
        showACL
            where hint = "<ACL name to configure>"

addRule :: Commands
addRule =
  custom "rule" "<integer rule priority>" parseACLRule always $ \(priority, ace) -> do
      aclName <- gets target >>= serialize
      db      <- gets db
      let entry = (pack $ show priority, pack $ show ace)
      setMapKVs db (aclKeyPrefix <> aclName) [ entry ]
      return NoAction

parseACLRule :: Parser StateM ACLRule
parseACLRule = parseWith $ parseRule aceParser

destroy :: Commands
destroy =
  command "destroy" "Remove this ACL from the system" $ do
    db      <- gets db
    aclName <- gets target >>= serialize
    del db [aclKeyPrefix <> aclName]
    return NoAction

delRule :: Commands
delRule =
  param "rule" "<priority of the rule to be removed>" asInt $ \priority -> do
    aclName <- gets target >>= serialize
    db      <- gets db
    delMapKVs db (aclKeyPrefix <> aclName) [pack $ show priority]
    return NoAction

showACLs :: Commands
showACLs =
    command "acls" "Show a list of ACLs configured" $ do
      showObjects aclKeyPrefix
      return NoAction

showACL :: Commands
showACL =
    command "show" "Show ACL contents" $ do
      aclName <- gets target >>= serialize
      showObject (aclKeyPrefix <> aclName) Nothing
      return NoAction
