{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Hofund - Heimdall CLI
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Main where

import Control.Monad.Except          (runExceptT)
import Control.Monad.State.Strict    (evalStateT)
import Data.Default                  (def)
import Database.Adapter              (new)
import System.Console.StructuredCLI
import Hofund.Commands               (root)
import Hofund.Types                  (AppState(..))
import System.Environment            (getArgs)

main :: IO ()
main = do
  db <- runExceptT new >>= either dbError return
  args <- getArgs
  -- the moment we get more arguments, we will get fancier. For now just a quick
  -- and dirty way to enable batch mode in the CLI
  batch <- case args of
             [] ->
                return False
             ["--batch"] ->
                return True
             _ -> do
                putStrLn "usage: [--batch]"
                putStrLn "    --batch: Enable batch processing mode\n"
                error "Invalid arguments"
  result <- evalStateT (runCLI "Heimdall" settings { isBatch = batch } root) $
              AppState db "" "" "" False False False
  case result of
    Left Exit ->
        return ()
    Left e ->
        putStrLn $ "Unrecoverable exception: " ++ show e
    _ ->
        return ()
    where settings = def { getBanner = "Heimdall CLI\nTab completion is your friend. "
                                         ++ "Question mark (?) is your saviour.",
                           getHistory = Just ".hofund.history" }
          dbError err =
            error $ "Failed to connect to DB: " ++ show err
