{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Routing Policies
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Policies where

import Prelude hiding (drop, dropWhile, lookup, reverse, span)
import Control.Concurrent.Async    (Async, async)
import Control.Exception           (throw)
import Control.Monad               (mapM, void)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO)
import Data.ByteString.Char8       (ByteString, dropWhile, reverse, stripPrefix, unpack)
import Data.Char                   (isSpace)
import Data.Configurable           (deserialize)
import Data.IORef                  (newIORef)
import Data.List                   (sortOn)
import Data.Maybe                  (fromMaybe)
import Data.Monoid                 ((<>))
import Data.Referable              (Dict, Referable(..), delRef)
import Database.Adapter            (Adapter, getMap, new)
import Database.Adapter.Redis      (Redis)
import Heimdall.ACLs               (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces         (getIDB)
import Heimdall.ObjectModel        (monitor)
import Heimdall.RouteMaps          (getRouteMapRef)
import Heimdall.Types
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging       hiding (monitor)

import qualified Data.Map                         as Map

type PolicyRefs = Dict ByteString Policy

{-# NOINLINE policies #-}
policies :: PolicyRefs
policies = unsafePerformIO $ newIORef Map.empty

getPolicyRef :: (MonadIO m) => ByteString -> m PolicyRef
getPolicyRef = getRef policies

getOrFetchPolicy :: (MonadIO m, Failable m) => PolicyRef -> m (Maybe Policy)
getOrFetchPolicy ref = deRef ref >>= getOrFetch
  where getOrFetch Nothing = do
          (db :: Redis) <- new -- this is horrible but there is no faster cleaner solution
          deRef =<< importPolicy db (policyKeyPrefix <> getRefName ref)
        getOrFetch policy = return policy

getPolicyMapEntry :: (MonadIO m) => (ByteString, ByteString) -> m PolicyMapEntry
getPolicyMapEntry (aclName, policyName) = do
  aclRef    <- getACLRef aclName
  policyRef <- getPolicyRef policyName
  return (aclRef, policyRef)

getPolicyMapRule :: (MonadIO m, Failable m) => (ByteString, ByteString) -> m PolicyMapRule
getPolicyMapRule (priorityStr, entryStr) = do
  priority              <- deserialize priorityStr
  (aclName, policyName) <- deserialize entryStr
  entry <- getPolicyMapEntry (trim aclName, trim policyName)
  return (priority, entry)
    where trim = snip . snip
          snip = reverse . dropWhile isSpace

updatePolicy :: (MonadIO m, Failable m) => ByteString -> Policy -> m PolicyRef
updatePolicy = updateRef policies

deletePolicy :: (MonadIO m) => ByteString -> m ()
deletePolicy = delRef policies

badAccess :: (MonadIO m) => String -> m ()
badAccess = throw . BadAccess

readPolicy :: (Adapter a, MonadIO m, Failable m) => a -> ByteString -> m Policy
readPolicy db name = do
  kvs     <- getMap db (policyKeyPrefix <> name)
  actions <- mapM (importRule name) kvs
  return Policy { policyName  = name,
                  policyRules = sortOn fst [ a | Just a <- actions ] }

importRule :: (MonadIO m) => ByteString -> (ByteString, ByteString) -> m (Maybe Rule)
importRule name (priorityStr, actionStr) = do
  rule <- runExceptT $ do
           priority <- deserialize priorityStr
           action   <- deserialize actionStr >>= instantiateAction
           return (priority, action)
  either badRule (return . Just) rule
      where badRule err = do
                warningM "Policies" $ "Invalid rule " ++ unpack priorityStr
                             ++ " in policy " ++ unpack name ++ ": " ++ unpack actionStr
                             ++ ": " ++ show err
                return Nothing

instantiateAction :: (MonadIO m, Failable m) => Action -> m Action
instantiateAction (RouteVia deadIDBRef) = do
  idb <- getIDB name
  return $ RouteVia idb
    where name = getRefName deadIDBRef
instantiateAction (Loadbalance deadRouteMapRef) = do
  routeMap <- getRouteMapRef routeMapName
  return $ Loadbalance routeMap
    where routeMapName = getRefName deadRouteMapRef
instantiateAction action =
  return action

runPolicies :: (?env::Env) => IO (Async())
runPolicies =
    async $ monitor "Policies" [policyKeyPrefix] policyChanged policyDeleted

impossible :: String -> a
impossible = throw . InternalError . (++ "Impossible condition: ")

policyNameFrom :: ByteString -> ByteString
policyNameFrom key = fromMaybe badKey $ stripPrefix policyKeyPrefix key
      where badKey = impossible $ "routing map change on malformed key " ++ unpack key

policyChanged :: (?env::Env, MonadIO m, Failable m) => ByteString -> m PolicyRef
policyChanged = importPolicy db
  where db = getDB ?env

policyDeleted :: (?env::Env, Failable m, MonadIO m) => ByteString -> m ()
policyDeleted  = void . importPolicy db
  where db = getDB ?env

importPolicy :: (Adapter a, Failable m, MonadIO m) => a -> ByteString -> m PolicyRef
importPolicy db key = do
  entries <- getMap db key
  case entries of
    [] ->
      deletePolicy name >> getPolicyRef name
    _  ->
      readPolicy db name >>= updatePolicy name
  where name = policyNameFrom key

importPolicyMap :: (?env::Env, MonadIO m, Failable m) => ByteString -> m PolicyMap
importPolicyMap key = do
  let policyMapName = key <> ".policies"
  policyRules <- getMap db policyMapName
  sortOn fst <$> mapM getPolicyMapRule policyRules
    where db = getDB ?env
