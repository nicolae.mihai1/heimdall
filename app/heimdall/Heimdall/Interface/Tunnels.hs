{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Tunnel Interfaces
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.Tunnels where

import Control.Applicative              ((<|>))
import Control.Concurrent               (threadDelay)
import Control.Concurrent.Async         (Async, AsyncCancelled, async)
import Control.Concurrent.MVar          (newEmptyMVar)
import Control.Exception                (Handler(..),
                                         SomeException,
                                         bracket,
                                         catches,
                                         throw)
import Control.Monad                    (forever, guard, void)
import Control.Monad.IO.Class           (MonadIO)
import Control.Monad.Except             (runExceptT)
import Control.Monad.Failable           (Failable(..))
import Control.Monad.Trans.Maybe        (MaybeT(..), runMaybeT)
import Data.ByteString.Char8            (ByteString, pack, unpack)
import Data.ByteString.Unsafe           (unsafePackCStringLen)
import Data.Configurable                (deserialize)
import Data.Maybe                       (isJust, fromMaybe)
import Data.Monoid                      ((<>))
import Heimdall.Common.Encapsulation
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces
import Heimdall.Interface.Tunnels.Types
import Heimdall.Interface.Utils         (getIPAddr, importIntf)
import Heimdall.Packet
import Heimdall.Routing                 (handleRxPacket)
import Heimdall.Types
import System.Logging

import qualified Heimdall.Interface.Tunnels.GRE as GRE

tunnelConfigPrefix :: ByteString
tunnelConfigPrefix = intfKeyPrefix <> "tunnel."

runTunnels :: (?env::Env) => IO (Async ())
runTunnels = async $ runInterfaces "Tunnels" [tunnelConfigPrefix] runTunnel stopTunnel

runTunnel :: (?env::Env) => ByteString -> [(ByteString, ByteString)] -> IO (Maybe (Async ()))
runTunnel key info = do
  tId <- async . forever $ serve name key info `catches` [
                      Handler $ \(e::AsyncCancelled) -> do
                        infoM "Tunnels" $ "Stopped service on " ++ show name
                                  ++ ": " ++ show e
                        throw e,
                      Handler $ \(e::SomeException) -> do
                        warningM "Tunnels" $ "Failure servicing "++ show name
                                     ++ ":" ++ show e
                        threadDelay 3000000
                     ]
  return $ Just tId
      where name   = fromMaybe badKey $ getIntfNameFromKey key
            badKey = throw . InvalidOrMissingConfig $
                       "Bad tunnel interface key: " ++ unpack key

serve :: (?env::Env) => IntfName -> ByteString -> [(ByteString, ByteString)] -> IO ()
serve name key info = bracket create destroy runService
    where create = do
            encap <- getEncap info
            let iD = getIntfID name
            infoM "Tunnels" $ "Initializing " ++ nameStr
            idb          <- getIDB name
            serviceAddr  <- runMaybeT $ getIPAddr info "service-address"
            listenerAddr <- runMaybeT $ getIPAddr info "listener-address"
            sockVar      <- newEmptyMVar
            return Tunnel { tId              = iD,
                            tIDB             = idb,
                            tName            = nameStr,
                            tEncap           = encap,
                            tIsLocal         = isLocalTunnel,
                            tServiceAddress  = serviceAddr,
                            tListenerAddress = listenerAddr <|> serviceAddr,
                            tForwarder       = tunnelForwarderFor encap,
                            tSockVar         = sockVar }
          destroy Tunnel{..} =
            infoM "Tunnels" $ "Shutting down " ++ nameStr
          runService tunnel = do
            let ?tunnel = tunnel
            importTunnel key name info >>= registerIntf
            if isLocalTunnel
               then service Rx
               else service Tx
          nameStr       = show name
          localNode     = pack $ getNode ?env
          isLocalTunnel = isJust $ do
                            tunnelNode <- lookup "node" info
                            guard $ tunnelNode == localNode
          tunnelForwarderFor GREType = GRE.txPacket

service :: (?tunnel::Tunnel) => XmitDirection -> IO ()
service dir = forever (service' encap) `catches` [
               Handler $ \(e::AsyncCancelled) -> do
                 infoM "Tunnels" $ show dir ++ " service for " ++ name
                           ++ " is being stopped"
                 throw e,
               Handler $ \(e::SomeException) -> do
                 warningM "Tunnels" $ show dir ++ " service for " ++ name
                           ++ " has terminated due to " ++ show e
                 threadDelay 3000000
              ]
    where name     = tName ?tunnel
          encap    = tEncap ?tunnel
          service' GREType = GRE.service dir

getEncap :: (MonadIO m, Failable m) => [(ByteString, ByteString)] -> m Encapsulation
getEncap info = do
  str <- maybe missingEncap return $ lookup "encapsulation" info
  deserialize str
      where missingEncap = failure $ InvalidValue "Missing encapsulation parameter"

stopTunnel :: ByteString -> IO ()
stopTunnel _key = return ()

importTunnel :: (?env::Env, ?tunnel::Tunnel)
                => ByteString
                -> IntfName
                -> [(ByteString, ByteString)]
                -> IO Interface
importTunnel key name info =
  importIntf key name info (txPacket ?tunnel) $ can RxData <> can TxData

txPacket :: Tunnel -> Packet -> Interface -> IO ()
txPacket tunnel@Tunnel{..} pkt@Packet{..} intf
    | tIsLocal =
        unsafePackCStringLen (getPktStart, fromIntegral getPktLen) >>=
          loopback
    | otherwise =
        tForwarder tunnel pkt intf
    where loopback = void . runExceptT . handleRxPacket tName tIDB rPath
          rPath    = failure . InvalidOrMissingConfig $ "Attempt to route via reverse path on loopback for " ++ tName
