{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall interface manipulation and helper functions
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.Utils where

import Control.Exception              (throw)
import Control.Monad                  (mapM)
import Control.Monad.Failable         (Failable, hoist)
import Control.Monad.IO.Class         (MonadIO)
import Control.Monad.Trans.Maybe      (MaybeT(..), runMaybeT)
import Data.ByteString.Char8          (ByteString, unpack)
import Data.Configurable              (deserialize)
import Data.IP                        (IP)
import Data.Maybe                     (fromMaybe)
import Heimdall.ACLs                  (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces            (getIDB, getIntf)
import Heimdall.Policies
import Heimdall.Types
import System.Logging
import Text.Read                      (readMaybe)

importIntf :: (?env::Env) => ByteString
                          -> IntfName
                          -> [(ByteString, ByteString)]
                          -> Forwarder
                          -> Capabilities
                          -> IO Interface
importIntf key name info forwarder capabilities = do
  policyMap  <- importPolicyMap key
  let vrfId        = fromMaybe 0 $ lookup "vrf-id" info >>= deserialize
      connTag      = lookup "connection-tag" info >>= deserialize
      masquerading = fromMaybe False $ lookup "masquerading" info >>= deserialize
      rPath        = lookup "rpath" info >>= deserialize
      logACL       = hoist id $ lookup "log-acl" info
  mLogACLRef <- runMaybeT $ getACLRef =<< logACL
  rpIDB      <- mapM getIDB rPath
  debugM "Interfaces" $ "Importing interface " ++ show name
  return Interface { _intfName         = name,
                     _intfVRFId        = vrfId,
                     _intfPolicyMap    = policyMap,
                     _intfCapabilities = capabilities,
                     _intfConnTag      = connTag,
                     _intfMasquerading = masquerading,
                     _intfRPIDB        = rpIDB,
                     _intfLogACL       = mLogACLRef,
                     _intfForwarder    = forwarder }

getIPAddr :: (Failable m) => [(ByteString, ByteString)] -> ByteString -> m IP
getIPAddr info property = do
  str <- hoist missing $ lookup property info
  deserialize str
      where missing () = InvalidOrMissingConfig $ unpack property

getIntfName :: ByteString -> ByteString -> IntfName
getIntfName typeStr idStr =
  let iD = fromMaybe badID $ readMaybe (unpack idStr)
      intfType = fromMaybe badType $ readMaybe (unpack typeStr)
  in IntfName (intfType, iD)
     where badID = throw . InvalidOrMissingConfig $ "Malformed " ++ unpack typeStr
                   ++ "interface ID " ++ unpack idStr
           badType =  throw . InvalidOrMissingConfig $ "Invalid interface type " ++ unpack typeStr
                   ++ '/':unpack idStr

selectRP :: (MonadIO m, Failable m) => IDB -> m IDB
selectRP idb = do
  intf <- getIntf idb
  selectRP' intf
  where selectRP' (Just Interface { _intfRPIDB = Just rPath}) = return rPath
        selectRP' _                                           = return idb
