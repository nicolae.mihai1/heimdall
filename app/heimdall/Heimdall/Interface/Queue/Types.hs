{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Interface.Queue.Types where

import Data.Word                     (Word16)
import Foreign.Ptr                   (Ptr)
import Heimdall.Types

data HuvudQueue

data Queue = Queue  { qId          :: Word16,
                      qDev         :: String,
                      qName        :: String,
                      qBpf         :: String,
                      qIDB         :: IDB,
                      qServiceType :: ServiceType,
                      qACL         :: Maybe ACLRef,
                      qHQ          :: Ptr HuvudQueue }

