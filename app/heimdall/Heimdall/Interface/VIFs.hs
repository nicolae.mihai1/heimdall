{-# LANGUAGE CPP, OverloadedStrings #-}
{- |
Description: Heimdall Virtual Interfaces
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.VIFs where

import Control.Concurrent          (threadDelay)
import Control.Concurrent.Async    (Async, AsyncCancelled, async)
import Control.Concurrent.MVar     (MVar, newEmptyMVar, putMVar, withMVar)
import Control.Exception           (Handler(..),
                                    SomeException,
                                    bracket,
                                    catches,
                                    throw)
import Control.Monad               (forever, void)
import Control.Monad.Failable      (failableIO)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Trans         (lift)
import Control.Monad.Trans.Maybe   (MaybeT(..), runMaybeT)
import Data.ByteString.Char8       (ByteString, pack, unpack)
import Data.ByteString.Unsafe      (unsafePackCStringLen)
import Data.Configurable           (deserialize)
import Data.IP                     (IP(..), toHostAddress, toHostAddress6)
import Data.Maybe                  (fromMaybe)
import Data.Monoid                 ((<>))
import Heimdall.Common.ObjectModel (getNameFromKey)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces
#ifdef LINUX
import Heimdall.Interface.VIF.Linux   (platformVIFStart, platformVIFStop)
#else
import Heimdall.Interface.VIF.Generic
#endif
import Heimdall.Interface.VIF.Types
import Heimdall.Interface.Utils
import Heimdall.Network.Foreign.Utils (bindToDevice, setConnectionTag)
import Heimdall.Packet
import Heimdall.Types
import Hexdump                     (prettyHex)
import Network.Socket
import Network.Socket.ByteString   (sendAllTo)
import System.Logging       hiding (monitor)
import Text.Read                   (readMaybe)

getVIFIntfName :: Int -> IntfName
getVIFIntfName = getIntfName "vif" . pack . show

vifConfigPrefix :: ByteString
vifConfigPrefix = intfKeyPrefix <> "vif."

runVIFs :: (?env::Env) => IO (Async ())
runVIFs = async $ runInterfaces "VIFs" [vifConfigPrefix] runVIF stopVIF

runVIF :: (?env::Env) => ByteString -> [(ByteString, ByteString)] -> IO (Maybe (Async ()))
runVIF key info = do
  let node = fromMaybe localNode $ lookup "node" info
  if localNode == node
    then runVIF'
    else return Nothing
        where runVIF' = do
                    tID <- async $ serve key info `catches` [
                           Handler $ \(e::AsyncCancelled) -> do
                             infoM "VIFs" $ "Stopped service for VIF " ++ vifId' ++ ": " ++ show e
                             throw e,
                           Handler $ \(e::SomeException) -> do
                             warningM "VIFs" $ "Failure servicing VIF " ++ vifId' ++ ": " ++ show e
                             threadDelay 3000000
                          ]
                    return $ Just tID
              vifId' = unpack $ getNameFromKey key
              localNode = pack $ getNode ?env

stopVIF :: ByteString -> IO ()
stopVIF _ = return ()

serve :: (?env::Env) => ByteString -> [(ByteString, ByteString)] -> IO ()
serve key info = bracket create destroy service >> threadDelay 2300000
    where create = do
            skVar <- newEmptyMVar
            idb   <- getIDB name
            intf  <- importIntf key name info (txPacket skVar) $ can TxData
            registerIntf intf
            let devName = lookup "device" info
                devType = lookup "type" info
                devPeer = lookup "peer" info
            devIPs     <- deserialize $ fromMaybe "" $ lookup "ip-addresses" info
            devPeerIPs <- deserialize $ fromMaybe "" $ lookup "peer-ip-addresses" info
            return VIF { vifId          = vifId',
                         vifDevName     = devName,
                         vifDevType     = devType,
                         vifDevPeer     = devPeer,
                         vifDevIPs      = devIPs,
                         vifDevPeerIPs  = devPeerIPs,
                         vifIDB         = idb,
                         vifSkVar       = skVar }
          destroy VIF{..} =
            infoM "VIFs" $ "Shutting down " ++ show name
          badVIFID = throw . InvalidOrMissingConfig $ "Bad VIF key (malformed ID): " ++ unpack key
          vifId'   = fromMaybe badVIFID $ readMaybe . unpack $ getNameFromKey key
          name     = fromMaybe badVIFID $ getIntfNameFromKey key

service :: VIF -> IO ()
service vif@VIF{..} = do
  infoM "VIFs" $ "Running service for " ++ name
  let initTx = runExceptT $ do
                 infoM "VIFs" $ "Configuring platform for " ++ name
                 failableIO $ platformVIFStart vif
                 v4Socket <- failableIO $ socket AF_INET Raw ipRawProto
                 v6Socket <- failableIO $ socket AF_INET6 Raw ipRawProto
                 void . runMaybeT $ do
                   devName <- MaybeT $ return vifDevName
                   let devName' = unpack devName
                   infoM "VIFs" $ "Binding " ++ name ++ " to " ++ devName'
                   lift $ do
                     bindToDevice v4Socket devName'
                     bindToDevice v6Socket devName'
                 void . runMaybeT $ do
                   Interface {..} <- getIntfM vifIDB
                   tag            <- MaybeT $ return _intfConnTag
                   infoM "VIFs" $ "Setting connection tag " ++ show tag ++ " for " ++ name
                   lift $ do
                     setConnectionTag v4Socket tag
                     setConnectionTag v6Socket tag
                 return $ DualSocket (v4Socket, v6Socket)
      stopTx (Left _) = return ()
      stopTx (Right (DualSocket (v4Socket, v6Socket))) = do
        infoM "VIFs" $ "Stopping service for " ++ name
        result <- runExceptT $
          failableIO $ do
            platformVIFStop vif
            close v4Socket
            close v6Socket
        either (failed "stopping VIF") return result
  bracket initTx stopTx $ either initFailure $ \sk -> do
    infoM "VIFs" $ "Starting " ++ name
    putMVar vifSkVar sk
    infoM "VIFs" $ name ++ " is ready"
    forever . threadDelay $ maxBound
      where initFailure err  =
              warningM "VIFs" $ "Unable to service VIF interface " ++ name ++ ": " ++ show err
            ipRawProto       = fromIntegral . fromEnum $ IP_Raw_Protocol
            name             = show $ getVIFIntfName vifId
            failed intro err = warningM "VIFs" $ "Failed " ++ intro ++ ": " ++ show err

txPacket :: MVar DualSocket -> Packet -> Interface -> IO ()
txPacket skVar pkt intf =
  withMVar skVar $ \sk ->
      let ?sk = sk in txPacket' pkt intf

txPacket' :: (?sk::DualSocket) => Packet -> Interface -> IO ()
txPacket' Packet{..} _intf =
    unsafePackCStringLen (getPktStart, fromIntegral getPktLen) >>=
                         xmit (destAddress getDstIP)
        where destAddress (IPv6 addr) = SockAddrInet6 defaultPort 0 (toHostAddress6 addr) 0
              destAddress (IPv4 addr) = SockAddrInet defaultPort (toHostAddress addr)
              sk = getSocketFor getFamily ?sk
              xmit addr bytes = do
                debugM "Network.Packets" $
                    show getSrcIP ++ " -> " ++ show getDstIP ++ ' ':prettyHex bytes
                sendAllTo sk bytes addr

getSocketFor :: Family -> DualSocket -> Socket
getSocketFor AF_INET  = fst . unwrapDualSocket
getSocketFor AF_INET6 = snd . unwrapDualSocket
getSocketFor family   = throw $ UnsupportedAddressFamily family
