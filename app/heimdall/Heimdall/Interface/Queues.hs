{-# LANGUAGE CPP, InterruptibleFFI, OverloadedStrings #-}

module Heimdall.Interface.Queues where

import Control.Applicative              (empty)
import Control.Concurrent               (threadDelay)
import Control.Concurrent.Async         (Async, AsyncCancelled, async)
import Control.Exception                (SomeException,
                                         Handler(..),
                                         bracket,
                                         catch,
                                         catches,
                                         mask,
                                         throw)
import Control.Monad                    (forM_, forever, void, when)
import Control.Monad.Except             (runExceptT)
import Control.Monad.Trans.Maybe        (MaybeT(..), runMaybeT)
import Data.ByteString.Char8            (ByteString, pack, unpack)
import Data.Configurable                (deserialize)
import Data.Maybe                       (fromMaybe)
import Data.Monoid                      ((<>))
import Data.Referable                   (ReferableException, watchRef)
import Data.Word                        (Word16)
import Foreign.C.String                 (CString, withCString)
import Foreign.Ptr                      (Ptr, nullPtr)
import Foreign.StablePtr                (StablePtr, newStablePtr, deRefStablePtr, freeStablePtr)
import Foreign.Storable                 (peek)
#ifdef LINUX
import Heimdall.Interface.Queue.Linux   (platformQueueStart, platformQueueStop)
#else
import Heimdall.Interface.Queue.Generic
#endif
import Heimdall.Interface.Queue.Types
import Heimdall.ACLs                    (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces
import Heimdall.Interface.Utils         (importIntf, selectRP)
import Heimdall.Packet                  (Packet)
import Heimdall.Routing                 (routePacket)
import Heimdall.Types
import System.Logging


import qualified Heimdall.Routing    as Routing

foreign import ccall "heimdall.h queue_init"    queue_init    :: Word16 ->
                                                                 CString ->
                                                                 CString ->
                                                                 CString ->
                                                                 IO (Ptr HuvudQueue)
foreign import ccall "heimdall.h queue_destroy" queue_destroy :: Ptr HuvudQueue -> IO ()
foreign import ccall interruptible "heimdall.h run_queue" run_queue :: StablePtr Queue ->
                                                                       Ptr HuvudQueue ->
                                                                       IO ()

foreign export ccall cAnalyze :: StablePtr Queue -> Ptr Packet -> IO Int

queueConfigPrefix :: ByteString
queueConfigPrefix = intfKeyPrefix <> "queue."

-- keep in sync with verdict_t in huvud.h
data Verdict = Invalid
             | AcceptPacket
             | DropPacket
             | StealPacket
             | WorkingOnIt
             | AbortProcessing
               deriving Enum

cAnalyze :: StablePtr Queue -> Ptr Packet -> IO Int
cAnalyze qPtr pktPtr = do
  q@Queue{..} <- deRefStablePtr qPtr
  analyze q `catch` \(e::SomeException) -> do
    infoM "Queues" $ "Failed to analyze packet on " ++ qName ++ ":" ++ show e
    return $ fromEnum AbortProcessing
  where analyze Queue{..} = do
          pkt     <- peek pktPtr
          action  <- routePacket qIDB (selectRP qIDB) pkt
          let verdict = verdictForAction action
          debugM "Queues" $ qName ++ " <- " ++ show pkt ++ ": " ++ show action
                     ++ '(':show verdict ++ ")"
          return verdict

verdictForAction :: Routing.Result -> Int
verdictForAction Routing.Bypass = fromEnum AcceptPacket
verdictForAction Routing.Drop   = fromEnum DropPacket
verdictForAction Routing.Routed = fromEnum DropPacket
verdictForAction Routing.Abort  = fromEnum AbortProcessing
verdictForAction Routing.None   = fromEnum DropPacket


runQueues :: (?env::Env) => IO (Async ())
runQueues = async $ runInterfaces "Queues" [queueConfigPrefix] runQ stopQ
    where runQ key info = do
            let node = fromMaybe localNode $ lookup "node" info
            if localNode == node
              then Just <$> runQ' key info
              else return Nothing
          runQ' key info = do
            let name = fromMaybe (badKey key) $ getIntfNameFromKey key
            infoM "Queues" $ "Running " ++ show name
            async $
              void . forever $ serve key info `catches` [
                           Handler $ \(e::ReferableException) ->
                             infoM "Queues" $ show name ++ " servicing restarted: " ++ show e,
                           Handler $ \(e::AsyncCancelled) -> do
                             infoM "Queues" $ show name ++ " servicing stopped: " ++ show e
                             throw e,
                           Handler $ \(e::SomeException) -> do
                             warningM "Queues" $ "Queue " ++ show name ++
                                          " servicing interrupted: " ++ show e
                             threadDelay 1500000]
          stopQ _ =
            return ()
          localNode = pack $ getNode ?env

serve :: (?env::Env) => ByteString -> [(ByteString, ByteString)] -> IO ()
serve key info =
  bracket initQ destroyQ $ \qPtr -> do
    queue@Queue{..} <- deRefStablePtr qPtr
    infoM "Queues" $ "Starting " ++ qName
    when (qHQ == nullPtr) $ do
      errorM "Queues" $ "Failed to create queue #" ++ (show qId) ++
                 ". Most likely cause is missing CAP_NET_ADMIN capability "
      throw NoQueueException
    let ?queue = queue
    intf <- importQ key info
    registerIntf intf
    bracket platformQueueStart platformQueueStop $ \_ -> do
      forM_ qACL $ watchRef mempty
      infoM "Queues" $ "Servicing " ++ qName ++ " @" ++ show qHQ
      run_queue qPtr qHQ
      infoM "Queues" $ "Exiting server for queue #" ++ show qId
        where initQ = mask $ \_ -> do
                aclRef <- maybe empty (fmap Just . getACLRef) $ lookup "acl" info
                idb <- getIDB name
                let dev  = unpack . fromMaybe "" $ lookup "device" info
                    bpf  = unpack . fromMaybe "" $ lookup "filter" info
                serviceType <- fromMaybe ForwardingService <$> getServiceType info
                withCString "Queues" $ \component ->
                  withCString dev $ \device ->
                    withCString bpf $ \filterExp -> do
                      let iD = fromIntegral $ getIntfID name
                      hq <- queue_init iD component device filterExp
                      newStablePtr Queue { qId          = iD,
                                           qDev         = dev,
                                           qBpf         = bpf,
                                           qName        = show name,
                                           qIDB         = idb,
                                           qServiceType = serviceType,
                                           qACL         = aclRef,
                                           qHQ          = hq }
              destroyQ qPtr = mask $ \_ -> do
                Queue{..} <- deRefStablePtr qPtr
                queue_destroy qHQ
                freeStablePtr qPtr
                infoM "Queues" $ "Destroyed " ++ qName
              name = fromMaybe (badKey key) $ getIntfNameFromKey key

importQ :: (?env::Env) => ByteString -> [(ByteString, ByteString)] -> IO Interface
importQ key info = do
  let name = fromMaybe (badKey key) $ getIntfNameFromKey key
  importIntf key name info (deadEnd $ show name) $ can RxData
      where deadEnd name = throw . InternalError $ "Packet hit a dead end on interface " ++ name

badKey :: ByteString -> IntfName
badKey key = throw . InvalidOrMissingConfig $ "Invalid queue interface config in " ++ unpack key

getServiceType :: [(ByteString, ByteString)] -> IO (Maybe ServiceType)
getServiceType info = runMaybeT $ do
  str <- MaybeT . pure $ lookup "service-type" info
  res <- runExceptT $ deserialize str
  MaybeT $ either (noParse str) (return . Just) res
      where noParse str err = do
              infoM "Queues" $ "Invalid service type " ++ show str ++ ": " ++ show err
              return Nothing
