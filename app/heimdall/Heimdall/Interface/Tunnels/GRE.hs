{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall GRE Tunnel Encapsulation
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interface.Tunnels.GRE where

import Control.Concurrent               (threadDelay)
import Control.Concurrent.MVar          (putMVar, withMVar)
import Control.Exception                (SomeException, bracket, bracket_, catch, throw)
import Control.Lens                     ((%~))
import Control.Monad                    (forever, void, when)
import Control.Monad.Except             (runExceptT, throwError)
import Control.Monad.Failable           (hoist)
import Control.Monad.Trans              (lift)
import Control.Monad.Trans.Maybe        (runMaybeT)
import Control.Monad.Trans.State.Strict (StateT(..), runStateT)
import Control.Monad.IO.Class           (liftIO)
import Data.Bifunctor                   (first)
import Data.Binary                      (Binary(..), encode, decodeOrFail)
import Data.Binary.Get                  (getWord16be)
import Data.Binary.Put                  (putWord16be)
import Data.Bits                        ((.|.), (.&.))
import Data.ByteString                  (ByteString)
import Data.ByteString.Unsafe           (unsafePackCStringLen)
import Data.Int                         (Int64)
import Data.IP                          (IP(..), toHostAddress)
import Data.Print                       (toString)
import Data.Referable                   (deRef)
import Data.Tuple.Select
import Data.Maybe                       (fromMaybe)
import Data.Word                        (Word32)
import Heimdall.Common.Encapsulation
import Heimdall.Exceptions
import Heimdall.Interfaces              (getIntf, getIntfM)
import Heimdall.Interface.Tunnels.Types (Tunnel(..))
import Heimdall.Network.Listeners       (getListenerRef, modifyListener, runListener, killListener)
import Heimdall.Packet                  (EtherType(..),
                                         Packet(..),
                                         Protocol(..),
                                         etherTypeFor)
import Heimdall.Routing                 (handleRxPacket)
import Heimdall.Types
import Hexdump                          (prettyHex)
import Network.Socket            hiding (recvFrom)
import Network.Socket.ByteString
import System.Logging


import qualified Data.ByteString                  as B
import qualified Data.ByteString.Lazy             as LB
import qualified Data.Map.Strict                  as Map
import qualified Network.Socket.ByteString.Lazy   as NL
import qualified Control.Monad.Trans.State.Strict as ST

data GREHeader = GREHeader { greHasChecksum     :: Bool,
                             greHasKey          :: Bool,
                             greHasSequence     :: Bool,
                             grePayloadProtocol :: EtherType }

instance Binary GREHeader where
    put GREHeader {..} = do
        let b1 = if greHasChecksum then 0x8000 else 0
            b2 = if greHasKey      then 0x2000 else 0
            b3 = if greHasSequence then 0x1000 else 0
        putWord16be $ b1 .|. b2 .|. b3
        putWord16be . fromIntegral . fromEnum $ grePayloadProtocol
    get = do
      h1 <- getWord16be
      let hasChecksum = h1 .&. 0x8000 /= 0
          hasKey      = h1 .&. 0x2000 /= 0
          hasSequence = h1 .&. 0x1000 /= 0
      h2 <- getWord16be
      return GREHeader { greHasChecksum     = hasChecksum,
                         greHasKey          = hasKey,
                         greHasSequence     = hasSequence,
                         grePayloadProtocol = toEnum . fromIntegral $ h2}

maxGREPacketSize :: Int
maxGREPacketSize = 9216 -- largest ethernet jumbo frame.. just in case and overkill probably
                        -- but whatever.. we just need to make sure an entire IP packet is
                        -- returned on a single socket read

minGREHeaderSize :: Int64
minGREHeaderSize = 4

service :: (?tunnel::Tunnel) => XmitDirection -> IO ()
service Rx = rxPackets
service Tx = txPackets

greProtocol :: ProtocolNumber
greProtocol = fromIntegral $ fromEnum GRE_Protocol

rxPackets :: (?tunnel::Tunnel) => IO ()
rxPackets = bracket_ acquireListener releaseListener $ forever $ threadDelay maxBound

getListenerName :: (?tunnel::Tunnel) => ListenerName
getListenerName =
  let Tunnel {..} = ?tunnel
      addr        = fromMaybe missingAddress tListenerAddress
  in (GREType, addr)

acquireListener :: (?tunnel::Tunnel) => IO ()
acquireListener = do
  runListener getListenerName $ do
    putMVar sockVar . throw . NetworkIOError $ "Can't send data on receive end of "
      ++ toString getListenerName
    bracket createSocket close $ rxGREPackets getListenerName
  Interface{..} <- fromMaybe missingInterface <$> getIntf idb
  modifyListener getListenerName $
    listenerVRFTable %~ Map.insert _intfVRFId ?tunnel
    where createSocket = do
            sk <- socket AF_INET Raw greProtocol
            setSocketOption sk ReuseAddr 1
            bind sk $ tunnelAddress tListenerAddress
            return sk
          sockVar = tSockVar ?tunnel
          idb     = tIDB ?tunnel

releaseListener :: (?tunnel::Tunnel) => IO ()
releaseListener = do
  void . runMaybeT $ do
    Interface {..} <- getIntfM idb
    modifyListener getListenerName $
      listenerVRFTable %~ Map.delete _intfVRFId
  killListener getListenerName
    where idb = tIDB ?tunnel

rxGREPackets :: ListenerName -> Socket -> IO ()
rxGREPackets name sk = do
  ref <- getListenerRef name
  Listener {..} <- fromMaybe (vanishedRef name) <$> deRef ref
  infoM "GRE" $ "Start reception of GRE traffic on " ++ (show $ listenerAddressFrom name)
  let ?ref  = ref
      ?sk   = sk
      ?name = name
  serve

serve :: (?name :: ListenerName, ?ref::ListenerRef Tunnel, ?sk :: Socket) => IO ()
serve = do
  (envelope, sender) <- recvFrom ?sk maxGREPacketSize
  debugM "GRE" $ show sender ++ " -> " ++ prettyHex envelope
  let ipHeaderLength = 4 * fromIntegral (B.head envelope .&. 0x0F)
      bytes          = B.drop ipHeaderLength envelope
  status' <- runExceptT $ do
              ((mVrfId, _seqNum), payload) <- hoist NetworkIOError $ decapsulate . LB.fromStrict $ bytes
              let vrfId = fromMaybe 0 mVrfId
              mListener <- deRef ?ref
              let Listener {..} = fromMaybe (vanishedRef ?name) $ mListener
              Tunnel {..}   <- hoist (missingTunnel vrfId sender) $ Map.lookup vrfId _listenerVRFTable
              let pkt =  LB.toStrict payload
              debugM "GRE" $ "Demux " ++  (show $ B.length pkt) ++ " bytes on " ++ show vrfId ++ " to " ++ tName
              -- note .. this is not really using the tunnel interface configured rpath.. fix this properly
              -- by unifying this logic accross all interfaces.. it doesn't make sense to do this outside of
              -- routing.. or does it?
              lift $ handleRxPacket tName tIDB (return tIDB) pkt `catch` \e ->
                infoM "GRE" $ "Failure routing packet: " ++ show (e::SomeException)
  debugM "GRE" $ "Done routing GRE pkt from " ++ show sender ++ ": " ++ show status'
  either failed return status'
  serve
    where failed err =
            debugM "GRE" $ "Rx processing on " ++ toString ?name ++ " failed: " ++ show err
          missingTunnel _ vrfId sender =
            InvalidOrMissingConfig $ "Discarding packet from " ++ show sender ++
                             " on VRF " ++ show vrfId ++ " sent to " ++ toString ?name

vanishedRef :: ListenerName -> a
vanishedRef name = throw . BadAccess  $ "reference to " ++ toString name ++ " has vanished"

missingAddress :: (?tunnel::Tunnel) => a
missingAddress = throw . InvalidOrMissingConfig $ "Bad or missing tunnel address for "
                   ++ tName ?tunnel

missingInterface :: (?tunnel :: Tunnel) => a
missingInterface = throw . InvalidOrMissingConfig $ "No interface for " ++ tName ?tunnel

txPackets :: (?tunnel::Tunnel) => IO ()
txPackets = bracket initTx stopTx $ either (failure "initialization") $ \sk -> do
              infoM "GRE" $ "Transmitting packets on " ++ tName ?tunnel
              putMVar sockVar sk
              forever $ threadDelay maxBound
    where initTx = runExceptT . liftIO $ do
                     sk <- socket AF_INET Raw greProtocol
                     connect sk $ tunnelAddress tServiceAddress
                     return sk
          stopTx (Right sk) =
              void . runExceptT . liftIO $ close sk
          stopTx _ = return ()
          name     = tName ?tunnel
          sockVar  = tSockVar ?tunnel
          failure str msg =
              infoM "GRE" $ "Failure during " ++ str ++ " of " ++ name ++ ": " ++ msg


-- Linux does not support IPv6 raw sockets it seems :-S . Ok to be fair this is
-- not just a Linux thing I think but rather an RFC3542 thing:
-- ... [The] difference from IPv4 raw sockets is that complete packets (that is, IPv6 packets
-- with extension headers) cannot be sent or received using the IPv6 raw sockets API...
tunnelAddress :: (?tunnel::Tunnel) => (Tunnel -> Maybe IP) -> SockAddr
tunnelAddress selectAddress = fromMaybe missingAddress $ do
                  addr <- v4AddressFrom <$> selectAddress ?tunnel
                  return $ SockAddrInet defaultPort $ toHostAddress addr
                      where v4AddressFrom (IPv6 _) = throw $ UnsupportedAddressFamily AF_INET6
                            v4AddressFrom (IPv4 a) = a

txPacket :: Tunnel -> Packet -> Interface -> IO ()
txPacket Tunnel{..} pkt@Packet{..} intf =
  void $ unsafePackCStringLen (getPktStart, fromIntegral getPktLen) >>=
    sendOne . encapsulate pkt intf
      where sendOne segment = do
              debugM "GRE" $ tName ++ " -> "++ prettyHex (LB.toStrict segment)
              withMVar tSockVar $ flip NL.sendAll segment

decapsulate :: LB.ByteString -> Either String ((Maybe Word32, Maybe Word32), LB.ByteString)
decapsulate bytes = do
  when (LB.length bytes < minGREHeaderSize) $ throwError "GRE header is too small"
  (rest, _,  GREHeader{..}) <- wrapFailure $ decodeOrFail bytes
  flip runStateT rest $ do
    cksum <- getOption greHasChecksum
    lift $ verify cksum
    key    <- getOption greHasKey
    seqNum <- getOption greHasSequence
    return (key, seqNum)
        where getOption True = ST.get >>= fmap Just . decodeS
              getOption False =
                return Nothing
              decodeS input = do
                (s, _, a) <- lift . wrapFailure $ decodeOrFail input
                ST.put s
                return a
              wrapFailure = first sel3

encapsulate :: Packet -> Interface -> ByteString -> LB.ByteString
encapsulate Packet{..} Interface {..} = LB.fromChunks . (hdr :) . (key :) . pure
    where hdr = LB.toStrict $ encode GREHeader { greHasKey          = True,
                                                 greHasChecksum     = False,
                                                 greHasSequence     = False,
                                                 grePayloadProtocol = etherTypeFor getFamily }
          key = LB.toStrict $ encode _intfVRFId

verify :: Maybe Word32 -> Either String ()
verify Nothing  = return ()
verify (Just _) = throwError "GRE checksum validation is not yet supported"
