{-# LANGUAGE OverloadedStrings #-}
module Heimdall.Interface.VIF.Linux where

import Control.Monad                   (void, when)
import Control.Monad.Failable          (failableIO)
import Control.Monad.Writer            (WriterT, execWriterT, lift, tell)
import Control.Monad.Trans.Maybe       (MaybeT(..), runMaybeT)
import Control.Monad.IO.Class          (MonadIO, liftIO)
import Data.ByteString.Char8           (ByteString, unpack)
import Data.Maybe                      (fromMaybe)
import Data.Print                      (toString)
import Heimdall.Types
import Heimdall.Interfaces             (getIntf)
import Heimdall.Interface.VIF.Types
import System.Exit                     (ExitCode(..))
import System.Logging
import System.Process                  (callCommand, runCommand, waitForProcess)
import System.Timeout                  (timeout)

getTable :: String
getTable = "nat"

getChain :: String
getChain = "POSTROUTING"

isMasquerading :: (?vif::VIF, MonadIO m) => m Bool
isMasquerading = do
  let VIF {..} = ?vif
  mIntf <- liftIO $ getIntf vifIDB
  return $ fromMaybe False $ mIntf >>= return . _intfMasquerading

getConnTag :: (?vif::VIF, MonadIO m) => m (Maybe ConnectionTag)
getConnTag = do
  let VIF {..} = ?vif
  mIntf <- liftIO $ getIntf vifIDB
  return $ _intfConnTag =<< mIntf

getDev :: (?vif::VIF, Monad m) => (VIF -> Maybe ByteString) -> m (Maybe String)
getDev getter = return $ getter ?vif >>= return . unpack

(<+>) :: (Monad m) => WriterT String m () -> String -> WriterT String m ()
(<+>) actions str = actions >> tell (' ':str)

baseCmd :: (Monad m) => String -> WriterT String m ()
baseCmd oper =
  tell "sudo iptables -t" <+> getTable <+> oper <+> getChain

masqueradingRule :: (?vif::VIF) => String -> WriterT String IO ()
masqueradingRule oper = do
    baseCmd oper
    getConnTag >>= perhaps "-m mark --mark "
    getDev vifDevName >>= maybe (avoid "lo") (say "-o")
    tell " -j MASQUERADE"
        where perhaps _ Nothing    = return ()
              perhaps arg (Just x) = say arg x
              avoid :: String -> WriterT String IO ()
              avoid   = say "! -o"
              say arg = tell . ((' ':arg) ++) .  (' ':) . toString

platformVIFStart :: VIF -> IO ()
platformVIFStart vif= do
  let ?vif = vif
  platformVIFStop vif -- für alle Fälle
  masquerading <- isMasquerading
  when masquerading $ iptables $ masqueradingRule "-A"
  void . runMaybeT $ do
    devName <- MaybeT $ getDev vifDevName
    devType <- MaybeT $ getDev vifDevType -- if type == Nothing, no further hw config
                                          -- will be performed
    (namespace, peer) <- if devType == "veth" then do
                           peer <- MaybeT $ getDev vifDevPeer
                           case span (/= ':') peer of
                             (x, "") ->
                               return ("", x)
                             (ns, x) ->
                               return (ns, dropWhile (== ':') x)
                         else
                           return ("", "")
    let extra  = if peer /= "" then " peer name " ++ peer else ""
        peerUp = " ip link set " ++ peer ++ " up"
        peerIPs = vifDevPeerIPs vif
        devIPs  = vifDevIPs vif
    lift $ do
      callCommand $ "sudo ip link add " ++ devName ++ " type " ++ devType ++ extra
      if namespace /= "" then do
        callCommand $ "sudo ip link set " ++ peer ++ " netns " ++ namespace
        callCommand $ "sudo ip netns exec " ++ namespace ++ peerUp
        cfgIPs ("sudo ip netns exec " ++ namespace) peer peerIPs
      else
        when (peer /= "") $ do
          callCommand $ "sudo" ++ peerUp
          cfgIPs "sudo" peer peerIPs
      callCommand $ "sudo ip link set " ++ devName ++ " up"
      cfgIPs "sudo" devName devIPs
      where cfgIPs with devName   = mapM_ $ cfgIP with devName
            cfgIP with devName ip = callCommand $ with ++ " ip addr add " ++ show ip ++ " dev " ++ devName

platformVIFStop :: VIF -> IO ()
platformVIFStop vif = do
  let ?vif = vif
  masquerading <- isMasquerading
  when masquerading $ iptables $ masqueradingRule "-D"
  void . runMaybeT $ do
    devName <- MaybeT $ getDev vifDevName
    failableIO . callCommand $ "sudo ip link delete " ++ devName

iptables :: WriterT String IO () -> IO ()
iptables actions = do
  cmd <- execWriterT actions
  debugM "VIF.IPTables" $ "Issuing " ++ cmd
  result <- timeout 2000000 $ runCommand cmd >>= waitForProcess
  when (result /= Just ExitSuccess) $
       infoM "VIF.IPTables" $ "Command " ++ cmd ++ " failed: " ++ show result
