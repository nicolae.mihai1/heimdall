module Heimdall.Interface.VIF.Generic where

import Heimdall.Interface.VIF.Types

platformVIFStart :: VIF -> IO ()
platformVIFStart _ = return ()

platformVIFStop :: VIF -> IO ()
platformVIFStop _ = return ()
