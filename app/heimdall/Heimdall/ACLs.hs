{-# LANGUAGE OverloadedStrings #-}
module Heimdall.ACLs where
{- |
Description: Heimdall ACLs management
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
import Control.Concurrent.Async    (Async, async)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO)
import Control.Monad.Trans.Maybe   (MaybeT(..))
import Data.Bifunctor              (first, second)
import Data.ByteString.Char8       (ByteString, unpack)
import Data.Either                 (isLeft, lefts, rights)
import Data.List                   (partition, sortOn)
import Data.Referable              (Dict, Referable(..))
import Database.Adapter            (Adapter, new, getMap)
import Database.Adapter.Redis      (Redis)
import Heimdall.Common.ACL         (parseACE)
import Heimdall.Common.ObjectModel (getNameFromKey)
import Heimdall.Types
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel        (monitor)
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging       hiding (monitor)
import Text.Read                   (readEither)

type ACLs = Dict ACLName ACL

{-# NOINLINE acls #-}
acls :: ACLs
acls = unsafePerformIO newDict

getACLRef :: (MonadIO m) => ACLName -> m ACLRef
getACLRef = getRef acls

getACLM :: (MonadIO m, Failable m) => ACLName -> MaybeT m ACL
getACLM = MaybeT . getACL

getACL :: (MonadIO m, Failable m) => ACLName -> m (Maybe ACL)
getACL = (=<<) deRef . getACLRef

getOrFetchACL :: (MonadIO m, Failable m) => ACLRef -> m (Maybe ACL)
getOrFetchACL ref = deRef ref >>= getOrFetch
  where getOrFetch Nothing = do
          (db :: Redis) <- new -- this is horrible but there is no faster cleaner solution
          deRef =<< importACL db (aclKeyPrefix <> getRefName ref)
        getOrFetch acl = return acl

runACLs :: (?env::Env) => IO (Async ())
runACLs = async $ monitor "ACLs" [aclKeyPrefix] aclChanged aclDeleted

aclChanged :: (?env::Env) => ByteString -> IO ACLRef
aclChanged = importACL db
    where db = getDB ?env

importACL :: (Adapter a, MonadIO m, Failable m) => a -> ByteString -> m ACLRef
importACL db key = do
  aces <- getMap db key
  rules <- mapM (runExceptT . importACERule) aces
  let (invalids, valids) = first lefts . second rights $ partition isLeft rules
  case invalids of -- SomeException is not an instance of Eq :-(
    [] ->
       return ()
    _ ->
       warningM "ACLs" $ "Invalid ACEs in ACL " ++ unpack name ++ ": " ++ show invalids
  updateRef acls name ACL { aclName  = name,
                            aclRules = sortOn fst valids }
    where name = getNameFromKey key

aclDeleted :: (?env::Env) => ByteString -> IO ()
aclDeleted key = do
  aces <- getMap db key
  case aces of
    [] -> do
      let name = getNameFromKey key
      delRef acls name
    _ ->
      aclChanged key >> return ()
  where db = getDB ?env

importACERule :: (MonadIO m, Failable m) => (ByteString, ByteString) -> m ACLRule
importACERule (priorityStr, aceStr) = do
  priority <- either (failure . InvalidValue) return . readEither $ unpack priorityStr
  result   <- parseACE aceStr
  ace      <- either badACERule return result
  return (priority, ace)
      where badACERule ((_, hint):_) = failure . InvalidValue $ hint
            badACERule _             = failure . InvalidValue $ "bad ACE rule"
