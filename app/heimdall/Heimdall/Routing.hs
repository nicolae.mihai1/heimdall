{-# LANGUAGE CPP #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{- |
Description: Heimdall Routing
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Heimdall.Routing (Result(..), handleRxPacket, routePacket) where

import Control.Applicative          ((<|>))
import Control.Exception            (Exception, SomeException, toException)
import Control.Lens
import Control.Monad                (MonadPlus, guard, mapM_, msum, void, unless, when)
import Control.Monad.Except         (ExceptT(..), runExceptT)
import Control.Monad.Failable       (Failable(..), hoist)
import Control.Monad.IO.Class       (MonadIO, liftIO)
import Control.Monad.State.Strict   (StateT, evalStateT, get)
import Control.Monad.Trans          (lift)
import Control.Monad.Trans.Maybe    (MaybeT(..), runMaybeT)
import Data.Typeable                (Typeable)
import Data.ByteString.Char8        (ByteString, unpack)
import Data.Default                 (def)
import Data.Fn                      ((-.-))
import Data.Hashable                (hash)
import Data.IP                      (IP(..), makeAddrRange)
import Data.List                    (find)
import Data.Maybe                   (isJust, isNothing, fromMaybe)
import Data.Print                   (toString)
import Data.Referable               (deRef, getRefName)
import Data.Vector                  ((!))
import Heimdall.ACLs                (getOrFetchACL)
import Heimdall.Connections
import Heimdall.Connection.Tracking (lookupConnection, updateConnection)
import Heimdall.Exceptions   hiding (Exception)
import Heimdall.Foreign.Packet      (withUnsafeL3Packet)
import Heimdall.Injector            (inject)
import Heimdall.Interfaces
import Heimdall.Network.Analysis    (analyze)
--import Heimdall.Network.Filter      (evalFilter)
import Heimdall.Packet              (Packet(..), Protocol(..))
import Heimdall.Policies            (getOrFetchPolicy)
import Heimdall.RouteMaps           (fetchRouteMap, getOrFetchRouteMap)
import Heimdall.Types
import System.Logging

import qualified Data.IP.RouteTable        as RT
import qualified Data.IntMap               as IMap
import qualified Data.Vector               as V

data State = State { _interimResult :: Result,
                     _inputIntf     :: Interface,
                     _packet        :: Packet,
                     _doLog         :: Bool,
                     _connection    :: Maybe Connection,
                     _packetFlow    :: Maybe Flow,
                     _flowDirection :: Maybe Direction }

makeLenses ''State

type RoutingT = StateT State

data RoutingException = NoMatch
                      | ResultNotOverridable
                      deriving (Typeable, Show, Eq)

instance Exception RoutingException

setResult :: (MonadIO m) => Result -> RoutingT m ()
setResult result = do
  previous <- use interimResult
  when (isOverridable previous) $ do
    mLog "Processing" $ "Routing result is now " ++ show result
    interimResult .= result

routePacket :: (MonadIO m, Failable m, MonadPlus m) => IDB -> IO IDB -> Packet -> m Result
routePacket idb rPath pkt = do
  mIntf <- getIntf idb
  intf  <- maybe (failed "missing or disabled ingress interface") return mIntf
  let state0 = State { _interimResult = def,
                       _inputIntf     = intf,
                       _packet        = pkt,
                       _doLog         = False,
                       _connection    = Nothing,
                       _packetFlow    = Nothing,
                       _flowDirection = Nothing }
  withState state0 $ routePacket' intf
    where routePacket' intf
              | intf <?> can RxData = do
                  let srcIP  = getSrcIP pkt
                      vrfID  = intf ^. intfVRFId
                      mVRFID = guard (vrfID /= 0) >> return vrfID
                  flow <- getPacketFlow pkt
                  mConn <- runMaybeT $
                    (MaybeT $ lookupConnection mVRFID flow) `recover` \_ -> do
                      rp <- liftIO rPath
                      MaybeT . updateConnection vrfID flow $ withConnection rp (vrfID, srcIP)
                  mDoLog <- runMaybeT $ do
                    aclRef  <- hoist id $ intf ^. intfLogACL
                    conn    <- hoist id mConn
                    ACL{..} <- MaybeT $ deRef aclRef
                    dir     <- lift $ calcDirection conn
                    hoist id $ matchACL intf dir pkt aclRules
                  connection .= mConn
                  doLog      .= isJust mDoLog
                  mLog "Incoming" $ show (intf ^. intfName) ++ " -> " ++ show pkt
                  let policyMap = intf ^. intfPolicyMap
                  void . runMaybeT . msum $ applyPolicies . snd <$> policyMap
                  result <- use interimResult
                  mLog "Outgoing" $ "Routing result: " ++ show result ++ " for " ++ show pkt
                  return result
              | otherwise = do
                  mLog "Incoming" $ "Discarding unexpected packet on transmit only interface: " ++ show pkt
                  return Drop
          withState               = flip evalStateT
          withConnection rp srcIP = return . (setSourceAddress srcIP) . fromMaybe (newConnection rp)
          setSourceAddress ip conn@Connection{..}
            | isNothing _source = conn & source ?~ ip
            | otherwise         = conn
          newConnection rp      = Connection {
                                    _source        = Nothing,
                                    _upAnalState   = def,
                                    _downAnalState = def,
                                    _rpIDB         = rp
                                  }

failed :: (MonadIO m, Failable m) => String -> m a
failed msg = do
  infoM "Routing" $ "Routing failure due to " ++ msg
  failure $ Aborted msg

missingConnection :: e -> SomeException
missingConnection _ = toException $ InternalError "Missing connection"

missingFlow :: e -> SomeException
missingFlow _ = toException $ InternalError "Missing flow"

mLog :: (MonadIO m) => String -> String -> RoutingT m ()
mLog section str = do
  logging <- use doLog
  when logging $ infoM ("Routing." ++ section) str

applyPolicies :: (MonadIO m, Failable m, MonadPlus m) => (ACLRef, PolicyRef) -> MaybeT (RoutingT m) ()
applyPolicies (aclRef, policyRef) = do
  lift . mLog "Processing" $ "Considering policy " ++ pName ++ " matching " ++ aName
  result <- use interimResult
  unless (isOverridable result) $ do
    lift . mLog "Processing" $ "Interim result " ++ show result ++ " is not overridable"
    failure ResultNotOverridable
  result' <- lift . runExceptT  $ guardACL aclRef
  either logFailure return result'
  lift $ applyPolicy =<< getOrFetchPolicy policyRef
      where applyPolicy (Just Policy{..}) = mapM_ applyRules policyRules
            applyPolicy Nothing           = do
              let policyName = unpack $ getRefName policyRef
              mLog "Processing" $ "Policy " ++ policyName ++ " not configured"
            pName = unpack $ getRefName policyRef
            aName = unpack $ getRefName aclRef
            logFailure e = do
              lift $ mLog "Processing" $ "Abort consideration of policy " ++ pName ++ ": " ++ show e
              failure e

isOverridable :: Result -> Bool
isOverridable Bypass = True
isOverridable Routed = True
isOverridable None   = True
isOverridable _      = False

matchACL :: Interface -> Direction -> Packet -> [ACLRule] -> Maybe Bool
matchACL intf dir = (shallPermit =<<) -.- matchingACE'
  where matchingACE'             = find . matchingACE intf dir
        shallPermit (_, ACE{..}) = return $ _aceAccess == Permit

guardACL :: (MonadIO m, Failable m) => ACLRef -> ExceptT SomeException (RoutingT m) ()
guardACL ref = do
  pkt     <- use packet
  inIntf  <- use inputIntf
  ACL{..} <- hoist missingACL =<< getOrFetchACL ref
  conn    <- hoist missingConnection =<< use connection
  dir     <- lift $ calcDirection conn
  flow    <- hoist missingFlow =<< use packetFlow
  let match = matchACL inIntf dir pkt aclRules
  lift $ mLog "Processing" $
    "Applying ACL " ++ unpack aclName  ++ " to " ++ toString flow ++ " connection: " ++
      toString conn ++ " on " ++ (show $ inIntf ^. intfName) ++ ": " ++ explainMatch match
  unless (fromMaybe False match) $ failure NoMatch
    where missingACL _ = InvalidOrMissingConfig $ name ++ " has not been configured"
          name         = unpack $ getRefName ref
          explainMatch (Just True)  = "matches with permit"
          explainMatch (Just False) = "matches with deny"
          explainMatch Nothing      = "doesn't match"

matchingACE :: Interface -> Direction -> Packet -> ACLRule -> Bool
matchingACE Interface{..} dir Packet{..} (_, ACE{..}) =
  let result = (_aceDirection >>= nothingIf (==) dir) <|>
               (_aceSrcRange  >>= nothingIfInRange getSrcIP) <|>
               (_aceDstRange  >>= nothingIfInRange getDstIP) <|>
               (_aceProtocol  >>= nothingIf protocolMatches getProto) <|>
               (_acePorts     >>= nothingIf portInRange (fromIntegral getDstPort)) <|>
               (_aceDevice    >>= nothingIf (==) (show _intfName))
      protocolMatches _ IP_Protocol = True
      protocolMatches a b = a == b
  in fromMaybe True result
      where nothingIfInRange ip@IPv4{} (IPRange range@(IPv4{},IPv4{})) = nothingIf inRange ip range
            nothingIfInRange ip@IPv6{} (IPRange range@(IPv6{},IPv6{})) = nothingIf inRange ip range
            nothingIfInRange _                  _                      = Just False
            inRange ip (rangeStart,rangeEnd) = rangeStart <= ip && ip <= rangeEnd
            nothingIf fun a b =
                if fun a b
                  then Nothing
                  else Just False

applyRules :: (MonadIO m, Failable m) => Rule -> RoutingT m ()
applyRules (_, RouteVia idb) = do
  result <- runMaybeT $ do
             intf <- getIntfM idb
             lift $ routeVia intf
  when (isNothing result) $ do
    mLog "Outgoing" $ "Dropping policy routed traffic out inactive or missing interface: " ++ show idb
    setResult Drop
      where routeVia outputIntf@Interface{..}
                | outputIntf <?> can TxData = do
                    State {..} <- get
                    mLog "Outgoing" $ "Routing packet " ++ (show $ getPktId _packet) ++ " -> " ++ show _intfName
                    liftIO $ sendPacket _packet _inputIntf outputIntf
                    setResult Routed
            routeVia Interface{..} | otherwise = do
              mLog "Outgoing" $ "Discarding unroutable packet to receive only interface " ++ show _intfName
              setResult Drop
applyRules (priority, Loadbalance routeMapRef) = do
  pkt   <- use packet
  flow  <- getPacketFlow pkt
  let i     = hash flow
      dstIP = getDstIP pkt
  result <- runMaybeT $ do
    conn  <- hoist missingConnection =<< use connection
    vrfID <- getVRFID conn
    routingMap <- MaybeT (getOrFetchRouteMap routeMapRef) `recover` \_ -> do
                    lift missingMap
                    failure $ InvalidOrMissingConfig (unpack $ getRefName routeMapRef)
    routeVia <- MaybeT . pure $ lookupRoute vrfID dstIP routingMap
    let numRoutes     = V.length routeVia
        selectedRoute = i `mod` numRoutes
        outputIntf    = routeVia ! selectedRoute
    return (priority, RouteVia outputIntf)
  maybe (noRoute dstIP) applyRules result
      where missingMap = do
              mLog "Outgoing" $ "Missing route map " ++ (unpack $ getRefName routeMapRef)
              setResult Drop
            noRoute dstIP = do
              mLog "Outgoing" $ "No route to " ++ (show dstIP) ++ " on route map "
                ++ (unpack $ getRefName routeMapRef) ++ ". Attempting route map re-fetch"
              void $ fetchRouteMap routeMapRef -- this should re fetch the routemap and
                                               -- even though it is too late for this pkt
                                               -- next one might hit a valid route if the
                                               -- route map was out of date
              setResult Drop
            lookupRouteMapTable vrfID RouteMap{..} =
              IMap.lookup (fromIntegral vrfID) routeMapTables
            lookupRoute vrfID (IPv4 ip) rMap = do
              v4Table <- fst <$> lookupRouteMapTable vrfID rMap
              RT.lookup (makeAddrRange ip 32) v4Table
            lookupRoute vrfID (IPv6 ip) rMap = do
              v6Table <- snd <$> lookupRouteMapTable vrfID rMap
              RT.lookup (makeAddrRange ip 128) v6Table
applyRules (priority, ApplyFilter filterRef) = do
  pkt   <- use packet
  conn  <- use connection >>= hoist missingConnection
  result <- runMaybeT $ do
    filtr@Filter {..}  <- MaybeT $ deRef filterRef
    conn' <- analyze conn filtr pkt
    connection ?= conn'
    lift $ do
      dir   <- calcDirection conn'
      flow  <- getPacketFlow pkt
      let analStatus = conn' ^. state dir . analysis . status
      mLog "Processing" $ "Flow " ++ toString flow ++ " connection: " ++ toString conn
        ++ " is " ++ show analStatus
      case analStatus of
        Undecisive ->
          applyRules (priority, _filterDefault)
        Incomplete ->
          setResult Bypass -- allow packet to flow for now until more data is gathered
        Partial | _filterWantsPartial ->
          applyFilter filtr conn'
        Partial | otherwise ->
          setResult Bypass
        Finished ->
          applyFilter filtr conn'
        Failed ->
          return ()
  maybe missingFilter return result
      where applyFilter _filtr _conn = do
              _pkt   <- use packet
              -- filter evaluation not yet implemented
              warningM "Routing.Processing" $ "Filter evaluation is not implemented - Bypassing connection"
              action <- return Continue -- evalFilter filtr conn pkt
              applyRules (priority, action)
            missingFilter =
              mLog "Processing" $ "Missing filter " ++ (unpack $ getRefName filterRef)
applyRules (priority, Forward)  = do
  conn <- use connection >>= hoist missingConnection
  dir <- calcDirection conn
  if dir == Downstream
    then applyRules (priority, RouteVia $ conn ^. rpIDB )
    else setResult Bypass
applyRules (_, Continue) = setResult Bypass
applyRules (_, Discard)  = setResult Drop

getPacketFlow :: (Monad m) => Packet -> RoutingT m Flow
getPacketFlow pkt = do
  pFlow  <- use packetFlow
  let calcFlow = flowFor pkt
      flow     = fromMaybe calcFlow pFlow
  when (isNothing pFlow) $ packetFlow ?= flow
  return flow

calcDirection :: (Monad m, Failable m) => Connection -> RoutingT m Direction
calcDirection conn = do
  pkt                <- use packet
  dir                <- use flowDirection
  figureOutDirection <- getDirection pkt conn
  let flowDir = fromMaybe figureOutDirection dir
  flowDirection .= Just flowDir
  return flowDir

handleRxPacket :: (MonadIO m, Failable m) => String -> IDB -> IO IDB -> ByteString -> m ()
handleRxPacket name idb rPath bytes = do
  result <- withUnsafeL3Packet bytes $ \pkt ->
    runExceptT $ do
      result' <- lift $ routePacket idb rPath pkt
      case result' of
        Abort ->
          failure . Aborted $ "Routing over interface " ++ name ++ " has been aborted"
        Bypass ->
          inject pkt 0
        _ ->
          return ()
  either failure return result
