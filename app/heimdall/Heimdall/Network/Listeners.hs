{-# LANGUAGE LambdaCase #-}

module Heimdall.Network.Listeners where

import Data.Referable              (Dict, Referable(..))
import Control.Concurrent.Async    (async, cancel)
import Control.Lens                ((&), (%~), (?~))
import Control.Monad               (when)
import Control.Monad.Failable      (Failable(..), hoist)
import Control.Monad.IO.Class      (MonadIO, liftIO)
import Control.Monad.Trans.Maybe   (MaybeT(..))
import Data.Print                  (toString)
import Heimdall.Exceptions
import Heimdall.Types.Listener
import System.IO.Unsafe
import System.Logging

type Listeners a = Dict ListenerName (Listener a)

{-# NOINLINE listeners #-}
listeners :: Listeners a
listeners = unsafePerformIO newDict

getListenerRef :: (MonadIO m) => ListenerName -> m (ListenerRef a)
getListenerRef = getRef listeners

getListenerM :: (MonadIO m, Failable m) => ListenerName -> MaybeT m (Listener a)
getListenerM = MaybeT . getListener

getListener :: (MonadIO m, Failable m) => ListenerName -> m (Maybe (Listener a))
getListener = (=<<) deRef . getListenerRef

modifyListener :: (MonadIO m, Failable m) => ListenerName -> (Listener a -> Listener a) -> m ()
modifyListener = modifyRef listeners

runListener :: (MonadIO m, Failable m) => ListenerName -> IO () -> m ()
runListener name server = do
  isNewListener <- mutateRef listeners name $
    \case
      Nothing ->
        newListener
      Just listener@Listener {..} ->
        if _listenerUsers == 0
          then newListener
          else (return $ listener & listenerUsers %~ (+1), False)
  when isNewListener $ startListener name server
    where listener0 =  Listener { _listenerName     = name,
                                  _listenerUsers    = 1,
                                  _listenerThread   = Nothing,
                                  _listenerVRFTable = mempty }
          newListener = (return listener0, True)

startListener :: (MonadIO m, Failable m) => ListenerName -> IO () -> m ()
startListener name server = do
  thread <- liftIO $ async $ do
    infoM "Listener" $ "Starting " ++ toString name ++ " listener"
    server
  modifyRef listeners name $ listenerThread ?~ thread

killListener :: (MonadIO m, Failable m) => ListenerName -> m ()
killListener name = do
    (mListener, shallKill) <- mutateRef listeners name $
      \case
        Nothing ->
          (Nothing, (Nothing, False)) -- already dead.. stays dead
        Just listener@Listener {..} | _listenerUsers == 1 ->
          (Nothing, (return listener, True))  -- last reference .. kill it
        Just listener ->
          (return $ listener & listenerUsers %~ subtract 1, (Nothing, False)) -- still in use.. commute sentence
    when shallKill $ do
        result <- runMaybeT $ do
          infoM "Listener" $ "Stopping " ++ nameStr ++ " listener..."
          Listener {..} <- MaybeT . return $ mListener
          thread        <- MaybeT . return $ _listenerThread
          liftIO $ cancel thread
          infoM "Listener" $ nameStr ++ " listener has been stopped"
          delRef listeners name
        hoist missingListener result
    where missingListener _ = MissingObject nameStr
          nameStr           = toString name
