{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Network Filters
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}


module Heimdall.Network.Filters where

import Data.ByteString.Char8             (ByteString, stripPrefix, unpack)
import Data.Configurable                 (deserializeOr, deserialize)
import Data.IORef                        (newIORef)
import Data.Maybe                        (fromMaybe)
import Data.Referable                    (Dict, Referable(..))
import Database.Adapter                  (getMap)
import Control.Concurrent.Async          (Async, async)
import Control.Exception                 (throw)
import Control.Lens                      ((.~), (&))
import Control.Monad                     (void)
import Control.Monad.Failable            (Failable)
import Control.Monad.IO.Class            (MonadIO, liftIO)
import Control.Monad.Trans               (lift)
import Control.Monad.Trans.Maybe         (MaybeT(..), runMaybeT)
import Heimdall.Connections
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel              (monitor)
import Heimdall.Network.Connection       (terminate)
import Heimdall.Packet                   (Packet(..))
import Heimdall.Policies                 (getPolicyRef)
import Heimdall.Types
import System.IO.Unsafe                  (unsafePerformIO)

import qualified Data.Map as Map

type FilterRefs = Dict ByteString Filter

{-# NOINLINE filters #-}
filters :: FilterRefs
filters = unsafePerformIO $ newIORef Map.empty

getFilter :: (MonadIO m, Failable m) => ByteString -> MaybeT m Filter
getFilter name = do
  result <- lift $ getRef filters name
  MaybeT $ deRef result

updateFilter :: (MonadIO m, Failable m) => ByteString -> Filter -> m FilterRef
updateFilter = updateRef filters

deleteFilter :: (MonadIO m) => ByteString -> m ()
deleteFilter = delRef filters

badAccess :: (MonadIO m) => String -> m a
badAccess = throw . BadAccess

runFilters :: (?env::Env) => IO (Async ())
runFilters = async $ monitor "Filters" [filterKeyPrefix] filterChanged filterDeleted

filterNameFrom :: ByteString -> ByteString
filterNameFrom key = fromMaybe badKey $ stripPrefix filterKeyPrefix key
    where badKey = throw . InternalError $ "filter change on malformed key " ++ unpack key

filterChanged :: (?env::Env, MonadIO m, Failable m) => ByteString -> m FilterRef
filterChanged key = do
  let name = filterNameFrom key
  void . runMaybeT $ do
    f@Filter {..} <- getFilter name
    terminate =<< MaybeT (pure _filterConnection)
    updateFilter name $ f & filterConnection .~ Nothing
  liftIO $ readFilter key >>= updateFilter name

filterDeleted :: (MonadIO m) => ByteString -> m ()
filterDeleted = liftIO . deleteFilter . filterNameFrom

readFilter :: (?env::Env, MonadIO m, Failable m) => ByteString -> m Filter
readFilter key = do
  let db = getDB ?env
      name = filterNameFrom key
  kvs   <- getMap db key
  importFilter name kvs

importFilter :: (MonadIO m, Failable m) => ByteString
                                        -> [(ByteString, ByteString)]
                                        -> m Filter
importFilter name info = do
  layer        <- deserializeOr TransportLayer . fromMaybe mempty $ lookup "layer" info
  defAction    <- deserializeOr Continue . fromMaybe mempty $ lookup "default-action" info
  wantsPartial <- deserializeOr True . fromMaybe mempty $ lookup "wants-partial" info
  delegate     <- runMaybeT . deserialize . fromMaybe mempty $ lookup "delegate" info
  policy       <- runMaybeT $ do
                   policyName <- MaybeT . return $ lookup "policy" info
                   getPolicyRef policyName
  return Filter { _filterName         = name,
                  _filterLayer        = layer,
                  _filterWantsPartial = wantsPartial,
                  _filterDefault      = defAction,
                  _filterPolicy       = policy,
                  _filterConnection   = Nothing,
                  _filterDelegate     = delegate }

evalFilter :: (MonadIO m, Failable m) => Filter -> Connection -> Packet -> m Action
evalFilter Filter{..} Connection{..} Packet{..} = error "not implemented yet"
