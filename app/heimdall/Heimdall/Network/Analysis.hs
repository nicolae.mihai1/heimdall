module Heimdall.Network.Analysis where

import Control.Lens                    ((^.), (.~), (.=), use)
import Control.Monad                   (when)
import Control.Monad.IO.Class          (MonadIO, liftIO)
import Control.Monad.State.Strict      (get, execStateT, evalStateT)
import Control.Applicative             ((<|>))
import Data.ByteString.Unsafe          (unsafePackCStringLen)
import Data.Fn                         ((-.-))
import Control.Monad.Failable          (Failable)
import Heimdall.Connections
import Heimdall.Network.Analysis.HTTP  (parseHTTP)
import Heimdall.Network.Analysis.TLS   (parseTLS)
import Heimdall.Packet
import Heimdall.Types
import System.Logging

import qualified Data.Attoparsec.ByteString.Char8 as A

analyze :: (Failable m, MonadIO m) => Connection -> Filter -> Packet -> m Connection
analyze conn Filter{..} pkt@Packet{..} = do
  dir <- getDirection pkt conn
  let analState = conn ^. state dir
  liftIO $ unsafePackCStringLen (getPayload, fromIntegral getAvailablePayload) >>= \bytes ->
    withState dir analState $ do
      buffer    .= bytes
      direction .= dir
      depth     .= _filterLayer
      result <- parse
      when (_filterLayer > TransportLayer) $ analyze'' result
      where analyze'' (A.Done _ result) =
              analysis .= result
            analyze'' (A.Partial continuation) = do
              continue .= return continuation
              analysis . status .= Incomplete
            analyze'' (A.Fail _ ctx msg) = do
              debugM "Network.Analysis" $ "Parse failure: " ++ show msg ++ ": " ++ show ctx
              continue .= Nothing
              analysis . status .= Failed
            withState  dir = saveState dir -.- flip execStateT
            saveState  dir = fmap $ saveState' dir
            saveState' dir = ($ conn) . (state dir .~)

parse :: (MonadIO m) => AnalST m (A.Result L457Analysis)
parse = do
  continuation <- use continue
  maybe parseL57 continueParsing continuation
        where continueParsing continuation = do
                bytes <- use buffer
                return $ continuation bytes

parseL57 :: (MonadIO m) => AnalST m (A.Result L457Analysis)
parseL57 = do
    bytes  <- use buffer
    state' <- get
    return $ A.parse (parseAny `evalStateT` state') bytes
        where parseAny = parseHTTP <|> parseTLS
