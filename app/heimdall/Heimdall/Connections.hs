{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes      #-}

module Heimdall.Connections where

import Control.Exception            (throw)
import Control.Lens
import Control.Monad.Failable       (Failable, hoist)
import Control.Monad.State.Strict   (StateT)
import Data.ByteString              (ByteString)
import Data.Default                 (Default(..))
import Data.IP                      (IP)
import Data.Print                   (Print(..))
import Heimdall.Exceptions
import Heimdall.Packet
import Heimdall.Types

import qualified Data.Attoparsec.ByteString.Char8 as A

data Connection = Connection { _source        :: Maybe (VRFID, IP),
                               _rpIDB         :: IDB,
                               _upAnalState   :: AnalState,
                               _downAnalState :: AnalState
                             }

data AnalState = AnalState {
                    _buffer    :: ByteString,
                    _direction :: Direction,
                    _depth     :: FilterDepth,
                    _continue  :: Maybe (ByteString -> A.Result L457Analysis),
                    _analysis  :: L457Analysis
                 }

instance Default AnalState where
    def = AnalState { _buffer    = mempty,
                      _depth     = ApplicationLayer,
                      _direction = Unknown,
                      _continue  = Nothing,
                      _analysis  = def }

instance Print Connection where
    toString Connection {..} =
        sourceIP ++ ' ':
        "RP: " ++ toString _rpIDB ++ ' ':
        upAnalysisState ++ '/':
        downAnalysisState
            where sourceIP = maybe "<new>"  show _source
                  upAnalysisState   = printAnalState _upAnalState
                  downAnalysisState = printAnalState _downAnalState
                  printAnalState _ = "<not implemented>"

type AnalST = StateT AnalState

getDirection :: (Failable m) => Packet -> Connection -> m Direction
getDirection Packet {..} Connection {..} = do
  connectionSrc <- hoist (const $ InternalError "Improperly setup connection") _source
  case connectionSrc of
    (_, ip) | getSrcIP == ip ->
      return Upstream
    _ ->
      return Downstream

makeLenses ''AnalState
makeLenses ''Connection

state :: Direction -> Lens' Connection AnalState
state Upstream   = upAnalState
state Downstream = downAnalState
state Unknown    = throw $ InternalError "No analysis state for unknown flow direction"

getVRFID :: (Failable m) => Connection -> m VRFID
getVRFID = fmap (view _1) . hoist (const $ InternalError "Missing connection VRF ID") . view source
