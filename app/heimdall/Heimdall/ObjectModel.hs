{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Object Model utility functions
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.ObjectModel where

import Prelude             hiding (span)
import Control.Monad.Except       (runExceptT)
import Control.Monad.Fix          (fix)
import Control.Monad.IO.Class     (MonadIO, liftIO)
import Control.Exception          (SomeException, catch, fromException, throw)
import Control.Monad.Failable     (Failable(..))
import Data.Monoid                ((<>))
import Data.ByteString.Char8      (ByteString, append, span, stripPrefix, unpack)
import Data.Configurable          (Configurable, deserialize)
import Data.IORef                 (IORef, atomicModifyIORef', newIORef)
import Data.List                  (any)
import Data.Maybe                 (catMaybes)
import Data.Map                   (Map)
import Data.Referable             (contextFromReferable)
import Data.Typeable              (Typeable)
import Database.Adapter           (Event(..),
                                   SubAction(..),
                                   get,
                                   searchAll,
                                   uponChange)
import Heimdall.Environment
import Heimdall.Exceptions
import System.Logging

import qualified Control.Exception as CE
import qualified Data.Map          as Map

data ObjectModelError = DeserializationFailure String
                      deriving (Typeable, Show)

instance CE.Exception ObjectModelError

monitor :: (?env::Env) => String -> [ByteString] -> (ByteString -> IO a) -> (ByteString -> IO ()) -> IO ()
monitor component prefixes changeFn deleteFn = do
  -- need to store the objects into this anchor map so that they don't get garbage collected
  anchors <- newIORef $ Map.empty
  let ?component = component
      ?changeFn  = changeFn
      ?deleteFn  = deleteFn
      ?anchors   = anchors
  monitor' prefixes

monitor' :: (?env::Env,
             ?anchors::IORef(Map ByteString a),
             ?component::String,
             ?changeFn::(ByteString -> IO a),
             ?deleteFn::(ByteString -> IO ())) => [ByteString] -> IO ()
monitor' prefixes =
  fix . (>>) $ monitor'' `catch` \(e::SomeException) ->
    infoM ?component $ "Monitoring event: " ++ show e
  where db             = getDB ?env
        wildcards = (`append` "*") <$> prefixes
        monitor'' = do
          keys <- noDupKeys prefixes . concat <$> mapM (searchAll db) wildcards
          liftIO $ mapM_ anchor keys
          watchForChanges
        watchForChanges = do
          infoM ?component $ "Waiting for changes upon " ++ show wildcards
          uponChange db wildcards reconfigure `catch` refException
        refException err = do
          case fromException err of
            Just exc -> do
              let key = contextFromReferable exc
              infoM ?component $ "Watched reference killed: reconfiguring " ++ unpack key
              anchor key
            _ ->
              throw err
          watchForChanges

reconfigure :: (?anchors::IORef(Map ByteString a),
                ?component::String,
                ?changeFn::(ByteString -> IO a),
                ?deleteFn::(ByteString -> IO ())) => Event -> IO SubAction
reconfigure ev = reconfigure' ev >> return Continue
    where reconfigure' (Changed _ key) = info key "change"   >> anchor key
          reconfigure' (Added   _ key) = info key "addition" >> anchor key
          reconfigure' (Deleted _ key) = info key "deletion" >> yank key
          reconfigure' _               = throw . InternalError $ "Unexpected event: " ++ show ev

info :: (?component::String) => ByteString -> String -> IO ()
info key ev = infoM ?component $ "Triggered " ++ ev ++ " in " ++ unpack key

anchor :: (?changeFn::(ByteString -> IO a), ?anchors::IORef (Map ByteString a))
            => ByteString -> IO ()
anchor key = do
  value <- ?changeFn key
  atomicModifyIORef' ?anchors $ \m -> (Map.insert key value m, ())

yank :: (?anchors::IORef(Map ByteString a), ?deleteFn::(ByteString -> IO ()))
            => ByteString -> IO ()
yank key = do
  ?deleteFn key
  atomicModifyIORef' ?anchors $ \m -> (Map.delete key m, ())

noDupKeys :: [ByteString] -> [ByteString] -> [ByteString]
noDupKeys prefixes = filter (not . isSubKey)
    where isSubKey key   = any hasSubProperty . catMaybes $ fmap (flip stripPrefix $ key) prefixes
          hasSubProperty = (/= "") . snd . span (/= '.')

deserializeVal :: (?env::Env, Configurable a, MonadIO m, Failable m)
                  => ByteString
                  -> ByteString
                  -> m a
deserializeVal prefix name = do
  result <- runExceptT . get db $ prefix <> name
  flip (either failed) result $ \str -> do
    eVal <- runExceptT $ deserialize str
    either failed return eVal
          where db         = getDB ?env
                failed err = failure $ DeserializationFailure $ show err
