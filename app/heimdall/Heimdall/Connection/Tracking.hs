{-# LANGUAGE OverloadedStrings #-}
module Heimdall.Connection.Tracking where


import Control.Concurrent.Async          (Async, async)
import Control.Concurrent.MVar           (MVar, modifyMVar_, modifyMVar, newMVar, withMVar)
import Control.Monad.Except              (runExceptT)
import Control.Monad.Failable            (Failable(..), hoist)
import Control.Monad.IO.Class            (MonadIO, liftIO)
import Control.Monad.Trans.Maybe         (MaybeT(..), runMaybeT)
import Data.ByteString.Char8             (ByteString, unpack)
import Data.Configurable                 (Configurable)
import Data.Default                      (def)
import Data.TTLHashTable                 (TTLHashTable)
import Data.IntMap                       (IntMap)
import Data.Maybe                        (fromJust, fromMaybe, isNothing)
import Heimdall.Common.ObjectModel       (getNameFromKey)
import Heimdall.Connections              (Connection)
import Heimdall.Environment
import Heimdall.ObjectModel              (deserializeVal, monitor)
import Heimdall.Types
import System.IO.Unsafe                  (unsafePerformIO)
import System.Logging             hiding (monitor)

import qualified Data.HashTable.ST.Basic as Basic
import qualified Data.TTLHashTable       as HT
import qualified Data.IntMap             as IMap

type HashTable = TTLHashTable Basic.HashTable

type ConnTable = HashTable Flow (IntMap Connection)

data Context = Context {
      connTableVar :: MVar ConnTable,
      paramsVar   :: MVar Parameters
    }

data Parameters = Parameters { ttl :: Int }

{-# NOINLINE context #-}
context :: Context
context = unsafePerformIO $ do
            let defaultTTL = 60000 -- 60s default connection timeout
            ht           <- HT.newWithSettings def { HT.defaultTTL = defaultTTL }
            connTableVar <- newMVar ht
            paramsVar    <- newMVar Parameters { ttl = defaultTTL }
            return Context { connTableVar = connTableVar,
                             paramsVar    = paramsVar }

run :: (?env::Env) => IO (Async ())
run =
  async $ monitor "Connection.Tracking" [connTrackKeyPrefix] configChange configChange

configChange :: (?env::Env) => ByteString -> IO ()
configChange key =
  update name
      where name = getNameFromKey key

reconfigureTableWith :: (HT.Settings -> HT.Settings) -> HashTable k v -> IO ()
reconfigureTableWith mutator ht = do
  settings <- HT.getSettings ht
  HT.reconfigure ht $ mutator settings

update :: (?env::Env) => ByteString -> IO ()
update name@"ttl" =
  updateTableSetting name updateTTL setTTL
      where updateTTL ttl s = s { HT.defaultTTL = ttl }
            setTTL ttl p    = p { ttl = ttl }
update name =
  warningM "Connection.Tracking" $ "Unknown configuration key " ++ unpack name

updateTableSetting :: (?env::Env, Configurable a)
                      => ByteString
                      -> (a -> HT.Settings -> HT.Settings)
                      -> (a -> Parameters -> Parameters)
                      -> IO ()
updateTableSetting name updateSettings updateParams = do
  result <- runExceptT $ do
    val <- deserializeVal connTrackKeyPrefix name
    let updateVal = reconfigureTableWith $ updateSettings val
    liftIO $ reconfigureTables updateVal
    return val
  flip (either failed) result $ \val -> do
      let Context {..} = context
      modifyMVar_ paramsVar $ return . updateParams val
          where failed e = warningM "Connection.Tracking" $
                             "Invalid configuration " ++ unpack name ++ ": " ++ show e

reconfigureTables :: (?env::Env)
                   => (ConnTable -> IO ())
                   -> IO ()
reconfigureTables = withMVar connTableVar
  where Context {..} = context

lookupConnection :: (MonadIO m, Failable m) => Maybe VRFID -> Flow -> m (Maybe Connection)
lookupConnection mVRFID flow = liftIO $ do
  let Context {..} = context
  withMVar connTableVar $ \connTable -> runMaybeT $ do
    vrfMap <- MaybeT $ HT.find connTable flow
    hoist id $ connIn vrfMap
      where connIn | isNothing mVRFID = fmap snd . IMap.lookupMin
                   | otherwise        = IMap.lookup  (fromIntegral $ fromJust mVRFID)

updateConnection :: (MonadIO m, Failable m)
                    => VRFID
                    -> Flow
                    -> (Maybe Connection -> Maybe Connection)
                    -> m (Maybe Connection)
updateConnection vrfID flow mutate = do
  let Context {..} = context
  result <- liftIO . modifyMVar connTableVar $ \connTable -> do
             result <- runExceptT $ HT.mutate connTable flow connection
             return (connTable, result)
  hoist id result
      where connection mVRFMap0 =
              let vrfMap0 = fromMaybe IMap.empty mVRFMap0
                  mConn0  = IMap.lookup vrfInt vrfMap0
                  mConn   = mutate mConn0
                  vrfMap  = case mConn of
                              Nothing   -> IMap.delete vrfInt vrfMap0
                              Just conn -> IMap.insert vrfInt conn vrfMap0
                  mVRFMap = if IMap.null vrfMap
                              then Nothing
                              else return vrfMap
              in (mVRFMap, mConn)
            vrfInt = fromIntegral vrfID
