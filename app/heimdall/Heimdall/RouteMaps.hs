{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{- |
Description: Heimdall Route Maps
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.RouteMaps where

import Control.Concurrent.Async    (Async, async)
import Control.Lens
import Control.Monad               (mapM)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO)
import Control.Monad.State.Strict  (execState, modify)
import Control.Monad.Trans.Maybe   (MaybeT(..))
import Data.Attoparsec.ByteString  (parseOnly)
import Data.ByteString.Char8       (ByteString, unpack)
import Data.Configurable           (Configurable(..))
import Data.IP                     (AddrRange, IPv4, IPv6)
import Data.IntMap                 (IntMap)
import Data.Maybe                  (catMaybes, fromMaybe)
import Data.Referable              (Dict, Referable(..))
import Data.Vector                 (Vector)
import Database.Adapter            (Adapter, getMap, new)
import Database.Adapter.Redis      (Redis)
import Heimdall.Common.ObjectModel (getNameFromKey)
import Heimdall.Interfaces         (getIDB)
import Heimdall.Types
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel        (monitor)
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging              (infoM)

import qualified Data.IP.RouteTable as RT
import qualified Data.IntMap        as IMap
import qualified Data.Vector        as Vector

type RouteMaps = Dict RouteMapName RouteMap

{-# NOINLINE routeMaps #-}
routeMaps :: RouteMaps
routeMaps = unsafePerformIO newDict

getRouteMapRef :: (MonadIO m) => RouteMapName -> m RouteMapRef
getRouteMapRef = getRef routeMaps

getRouteMapM :: (MonadIO m, Failable m) => RouteMapName -> MaybeT m RouteMap
getRouteMapM = MaybeT . getRouteMap

getRouteMap :: (MonadIO m, Failable m) => RouteMapName -> m (Maybe RouteMap)
getRouteMap = (=<<) deRef . getRouteMapRef

getOrFetchRouteMap :: (MonadIO m, Failable m) => RouteMapRef -> m (Maybe RouteMap)
getOrFetchRouteMap ref = deRef ref >>= getOrFetch
  where getOrFetch Nothing = fetchRouteMap ref
        getOrFetch routeMap = return routeMap

fetchRouteMap :: (MonadIO m, Failable m) => RouteMapRef -> m (Maybe RouteMap)
fetchRouteMap ref = do
  (db :: Redis) <- new -- this is horrible but there is no faster cleaner solution
  deRef =<< importRouteMap db (routeMapKeyPrefix <> getRefName ref)

runRouteMaps :: (?env::Env) => IO (Async ())
runRouteMaps =
    async $ monitor "RouteMaps" [routeMapKeyPrefix] routeMapChanged routeMapDeleted

routeMapChanged :: (?env::Env) => ByteString -> IO RouteMapRef
routeMapChanged = importRouteMap db
  where db = getDB ?env

importRouteMap :: (Adapter a, MonadIO m, Failable m) => a -> ByteString -> m RouteMapRef
importRouteMap db key = do
  entries  <- getMap db key
  v4Routes <- catMaybes <$> mapM parseRoute entries
  v6Routes <- catMaybes <$> mapM parseRoute entries
  let name = getNameFromKey key
  infoM "RouteMaps" $ "Update route map " ++ unpack name ++ " reference"
  updateRef routeMaps name RouteMap {
                        routeMapName   = name,
                        routeMapTables = importRouteTables v4Routes v6Routes }

routeMapDeleted :: (?env::Env) => ByteString -> IO ()
routeMapDeleted key = do
  entries <- getMap db key
  case entries of
    [] -> do
      let name = getNameFromKey key
      infoM "RouteMaps" $ "Route map " ++ unpack name ++ " is now empty"
      delRef routeMaps name
    _ ->
      routeMapChanged key >> return ()
  where db = getDB ?env

parseRoute :: (Configurable a, MonadIO m) => (ByteString, ByteString) -> m (Maybe (a, Vector IDB))
parseRoute (prefixStr, intfList) = do
  result <- runExceptT $ do
             prefix    <- deserialize prefixStr
             intfsStr  <- deserialize intfList
             intfNames <- mapM parse intfsStr
             intfs     <- Vector.fromList <$> mapM getIDB intfNames
             return (prefix, intfs)
  return $ either (const $ Nothing) return result
      where parse = either (failure . InvalidValue) return . parseOnly parseIntfName

importRouteTables :: [((VRFID, AddrRange IPv4), Vector IDB)] ->
                     [((VRFID, AddrRange IPv6), Vector IDB)] ->
                     IntMap RouteMapTables
importRouteTables v4Entries v6Entries = importRouteTables' `execState` IMap.empty
    where importRouteTables'           = importRoute _1 v4Entries >> importRoute _2 v6Entries
          importRoute selector entries = modify $ flip (foldl $ importRoute' selector) entries
          importRoute' selector routeMapTables ((vrfID, prefix), idbs) =
            let tables0 = fromMaybe (RT.empty, RT.empty) $ IMap.lookup (fromIntegral vrfID) routeMapTables
                tables  = over selector (RT.insert prefix idbs) tables0
            in IMap.insert (fromIntegral vrfID) tables routeMapTables

