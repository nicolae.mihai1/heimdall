with (import <nixpkgs> { system = builtins.currentSystem;});
{ ghc ? pkgs.ghc }:

haskell.lib.buildStackProject {
  inherit ghc;
  cacert       = "/";
  name         = "heimdall";

  buildInputs  =
    let os           = builtins.substring 7 (-1) builtins.currentSystem;
        linuxInputs  = if os == "linux" then
                         [ libnfnetlink libnetfilter_queue ]
                       else
                         [ ];
        commonInputs = [ redis libpcap ];
    in commonInputs ++ linuxInputs;
}
