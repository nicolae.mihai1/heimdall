name:        heimdall
version:     1.3.0.0
description: Highly Effective Intervention Manipulation and Data Analysis at the Link Layer
git:         https://gitlab.com/codemonkeylabs/heimdall
homepage:    https://gitlab.com/codemonkeylabs/heimdall#readme
bug-reports: https://github.com/codemonkeylabs/heimdall/issues
author:      Erick Gonzalez <erick@codemonkeylabs.de>
maintainer:  erick@codemonkeylabs.de
copyright:   2018-2019 Erick Gonzalez
license:     LGPL

extra-source-files:
    - README.md
    - ChangeLog.md
    - package.yaml

flags:
  debug:
    description: Enable debug messages (normally disabled due to performance considerations)
    default: false
    manual: true

dependencies:
    - async          >= 2.2.1 && < 2.3
    - attoparsec     >= 0.13 && < 0.14
    - base           >= 4.7 && < 5
    - binary         >= 0.8.6 && < 0.9
    - bytestring     >= 0.10.8 && < 0.11
    - clock          >= 0.7 && < 0.8
    - containers     >= 0.6 && < 0.7
    - failable       >= 1.2 && < 1.3
    - hashable       >= 1.2.7 && < 1.3
    - iproute        >= 1.7.7 && < 1.8
    - data-default   >= 0.7.1.1 && < 0.8
    - lens           >= 4.17 && < 4.18
    - mtl            >= 2.2.2 && < 2.3
    - network        >= 2.8 && < 3.1
    - structured-cli >= 2.5.0.1 && < 2.6
    - transformers   >= 0.5.5 && < 0.6
    - vector         >= 0.12 && < 0.13

when:
    - condition: flag(debug)
      then:
        cc-options:  -D__DEBUG__
        cpp-options: -D__DEBUG__
        ghc-options:
            - -O0
      else:
        ghc-options:
            - -O2

ghc-options:
    - -Wall
    - -Werror
    - -fno-warn-orphans
    - -fobject-code
    - -optP-Wno-nonportable-include-path

default-extensions:
    - BangPatterns
    - FlexibleContexts
    - ImplicitParams
    - RecordWildCards
    - ScopedTypeVariables

library:
  source-dirs: src
  dependencies:
    - conduit     >= 1.3.1 && < 1.4
    - connection  >= 0.2.8 && < 0.3
    - hedis       >= 0.10 && < 0.13
    - hslogger    >= 1.2 && < 1.3
    - time        >= 1.8.0.2 && < 1.10
  exposed-modules:
    - Control.Concurrent.Utils
    - Data.Configurable
    - Data.Fn
    - Data.Hashes.FNV1a
    - Data.Print
    - Data.Referable
    - Database.Adapter
    - Database.Adapter.Redis
    - Heimdall.Common.ACL
    - Heimdall.Common.Encapsulation
    - Heimdall.Common.ObjectModel
    - Heimdall.Exceptions
    - Heimdall.Network.Connection
    - Heimdall.Network.Utils
    - Heimdall.Packet
    - Heimdall.Types
    - Heimdall.Types.ACL
    - Heimdall.Types.Analysis
    - Heimdall.Types.Connection
    - Heimdall.Types.Interface
    - Heimdall.Types.Filter
    - Heimdall.Types.Flow
    - Heimdall.Types.Listener
    - Heimdall.Types.Network
    - Heimdall.Types.Policy
    - Heimdall.Types.RouteMap
    - System.Logging
  default-extensions:
    - FlexibleInstances
    - GeneralizedNewtypeDeriving
    - MultiParamTypeClasses
    - NamedFieldPuns

executables:
  heimdall:
    main:                Main.hs
    source-dirs:         app/heimdall
    ghc-options:
        - -threaded
        - -rtsopts
        - -with-rtsopts=-N
    dependencies:
        - heimdall
        - hashtables     >= 1.2.3 && < 1.3
        - pretty-hex     >= 1.0 && < 1.1
        - process        >= 1.6.2 && < 1.7
        - ttl-hashtables >= 1.3 && < 1.4
        - tuple          >= 0.3 && < 0.4
    other-modules:
        - Heimdall
        - Heimdall.ACLs
        - Heimdall.CLogging
        - Heimdall.Config
        - Heimdall.Connection.Tracking
        - Heimdall.Connections
        - Heimdall.Environment
        - Heimdall.Foreign.Packet
        - Heimdall.Interface.Queue.Generic
        - Heimdall.Interface.Queue.Linux
        - Heimdall.Interface.Queue.Types
        - Heimdall.Interface.Queues
        - Heimdall.Interface.Tunnels
        - Heimdall.Interface.Tunnels.Types
        - Heimdall.Interface.Tunnels.GRE
        - Heimdall.Interface.Utils
        - Heimdall.Interface.VIF.Linux
        - Heimdall.Interface.VIF.Generic
        - Heimdall.Interface.VIF.Types
        - Heimdall.Interface.VIFs
        - Heimdall.Interfaces
        - Heimdall.Injector
        - Heimdall.Network.Analysis
        - Heimdall.Network.Analysis.HTTP
        - Heimdall.Network.Analysis.TLS
        - Heimdall.Network.Filters
        - Heimdall.Network.Foreign.Utils
        - Heimdall.Network.Listeners
        - Heimdall.ObjectModel
        - Heimdall.Policies
        - Heimdall.RouteMaps
        - Heimdall.Routing
    default-extensions:
        - ExistentialQuantification
        - TupleSections
    c-sources:
        - native/src/heimdall.c
        - native/src/huvud.c
        - native/src/l3.c
        - native/src/l4.c
    include-dirs:
        - native/include
    when:
        - condition: os(linux)
          then:
            cc-options:      -DLINUX
            cpp-options:     -DLINUX
            c-sources:
                - native/src/nfq.c
            include-dirs:
                - native/include
            extra-libraries: netfilter_queue
          else:
            c-sources:
                - native/src/pcap.c
            include-dirs:
                - native/include
            extra-libraries: pcap

  hofund:
    main:                Main.hs
    source-dirs:         app/hofund
    ghc-options:
        - -threaded
        - -rtsopts
        - -with-rtsopts=-N
    dependencies:
        - heimdall
        - structured-cli >= 2.5.0.3 && < 2.6
    other-modules:
        - Hofund.Commands
        - Hofund.Commands.ACL
        - Hofund.Commands.Common
        - Hofund.Commands.Filter
        - Hofund.Commands.Interface
        - Hofund.Commands.Logging
        - Hofund.Commands.Policy
        - Hofund.Commands.RouteMap
        - Hofund.Types

tests:
  heimdall-test:
    main:                Spec.hs
    source-dirs:         test
    ghc-options:
    - -threaded
    - -rtsopts
    - -with-rtsopts=-N
    dependencies:
    - heimdall
