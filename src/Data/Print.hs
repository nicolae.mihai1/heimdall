module Data.Print where

import Data.ByteString.Char8  (ByteString, unpack)

class Print a where
    toString :: a -> String

instance Print String where
    toString = id

instance Print ByteString where
    toString = unpack
