{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE ExistentialQuantification #-}

{- |
Description: Heimdall Name-Referable class
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Data.Referable where

import Control.Concurrent        (ThreadId, myThreadId)
import Control.Exception         (Exception(..), SomeException, throw, throwTo)
import Control.Monad             (guard)
import Control.Monad.Failable    (Failable(..))
import Control.Monad.IO.Class    (MonadIO, liftIO)
import Data.ByteString.Char8     (ByteString, unpack)
import Data.Int                  (Int64)
import Data.IORef                (IORef, atomicModifyIORef', newIORef, readIORef)
import Data.Map                  (Map)
import Data.Maybe                (fromMaybe)
import Data.Print                (Print(..))
import Data.Set                  (Set)
import Data.Typeable             (Typeable)
import System.Logging
import System.Mem.Weak           (Weak, deRefWeak, mkWeakPtr)
import System.Utils              (timeSinceStart)

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Heimdall.Exceptions as HE

type Box a              = (Maybe a, Watches)
type BoxRef a           = IORef (Box a)
newtype VolatileRef k a = VolatileRef { unwrapVolatileRef :: (k, BoxRef a) }
type Watches            = Set Watch
type Watch              = (ThreadId, ByteString)
type TimeStamp          = Int64
type TSWPtr k v         = (TimeStamp, (Weak (VolatileRef k v)))
type Dict k v           = IORef (Map k (TSWPtr k v))

instance (Print k) => Show (VolatileRef k a) where
    showsPrec _ (VolatileRef (key, _)) = (('#':toString key) ++)

instance (Print k, Referable a) => Print (VolatileRef k a) where
    toString = toString . getRefName

unwrapBox :: Box a -> Maybe a
unwrapBox = fst

data ReferableException = forall k . (Show k) => RefChange k ByteString
                        | forall k . (Show k) => RefDelete k ByteString
                        | forall k . (Show k) => RefKilled k ByteString
                            deriving (Typeable)

contextFromReferable :: ReferableException -> ByteString
contextFromReferable (RefChange _ context) = context
contextFromReferable (RefDelete _ context) = context
contextFromReferable (RefKilled _ context) = context

instance Show ReferableException where
  show (RefChange k context) = "RefChange " ++ show k ++ ' ':unpack context
  show (RefDelete k context) = "RefDelete " ++ show k ++ ' ':unpack context
  show (RefKilled k context) = "RefKilled " ++ show k ++ ' ':unpack context

instance Exception ReferableException

class Referable a where

    newDict :: (Ord k, MonadIO m) => m (Dict k a)
    newDict = liftIO $ newIORef Map.empty

    deRef :: (MonadIO m, Print k) => VolatileRef k a -> m (Maybe a)
    deRef (VolatileRef (_, ref)) =
      liftIO $ unwrapBox <$> readIORef ref

    getRef :: (Ord k, Print k, MonadIO m) => Dict k a -> k -> m (VolatileRef k a)
    getRef dict name = liftIO $ do
      mVRefPtr <- readIORef dict >>= return . Map.lookup name
      mVRef    <- maybe newVRef (deRefWeak . snd) mVRefPtr
      maybe (getRef dict name) return mVRef
          where newVRef = do
                   bRef <- newIORef $ (Nothing, Set.empty)
                   let vRef = VolatileRef (name, bRef)
                   created <- timeSinceStart
                   vRefPtr <- mkWeakPtr vRef $ Just (killRef created)
                   atomicModifyIORef' dict $ \m ->
                       case Map.lookup name m of
                         Nothing ->
                             (Map.insert name (created, vRefPtr) m, return vRef)
                         Just _ ->
                             (m, Nothing)
                killRef created = do
                  released <- liftIO $ atomicModifyIORef' dict $ \m -> fromMaybe (m, False) $ do
                    (backWhen, _) <- Map.lookup name m
                    guard $ created == backWhen
                    return (Map.delete name m, True)
                  if released
                    then debugM "Referable" $ "Releasing reference to " ++ toString name
                    else debugM "Referable" $ "Skipping release of resurrected reference to " ++ toString name

    updateRef :: (Print k, Show k, Typeable k, Ord k, MonadIO m, Failable m)
                 => Dict k a
                 -> k
                 -> a
                 -> m (VolatileRef k a)
    updateRef dict name value = do
      debugM "Referable" $ "Updating reference to " ++ toString name
      vRef@(VolatileRef (_, bRef)) <- getRef dict name
      watches <- liftIO $ atomicModifyIORef' bRef updateRef'
      notify (RefChange name) watches
      return vRef
          where updateRef' (_, watches) = ((Just value, mempty), watches)

    delRef :: (Print k, Show k, Typeable k, Ord k, MonadIO m) => Dict k a -> k -> m ()
    delRef dict name = do
      debugM "Referable" $ "Deleting reference to " ++ toString name
      VolatileRef (_, bRef) <- getRef dict name
      watches <- liftIO $ atomicModifyIORef' bRef delRef'
      notify (RefDelete name) watches
          where delRef' (_, watches) = ((Nothing, mempty), watches)

    modifyRef :: (Print k, Ord k, Show k, Typeable k, MonadIO m, Failable m)
                 => Dict k a
                 -> k
                 -> (a -> a)
                 -> m ()
    modifyRef dict name fn = do
      debugM "Referable" $ "Modifying reference to " ++ toString name
      mutateRef dict name fn'
        where fn' v = (v >>= return . fn , ())

    mutateRef :: (Print k, Ord k, Show k, Typeable k, MonadIO m, Failable m)
                 => Dict k a
                 -> k
                 -> (Maybe a -> (Maybe a, r))
                 -> m r
    mutateRef dict name fn = do
      debugM "Referable" $ "Mutating reference to " ++ toString name
      VolatileRef (_, bRef) <- getRef dict name
      (watches, r) <- liftIO $ atomicModifyIORef' bRef mutateRef'
      notify (RefChange name) watches
      return r
          where mutateRef' (v, watches) =
                  let (v', r) = fn v
                  in ((v', mempty), (watches, r))

    watchRef :: (Print k, MonadIO m, Failable m) => ByteString -> VolatileRef k a -> m ()
    watchRef context (VolatileRef (k, bRef)) =
      liftIO $ do
        myself <- myThreadId
        debugM "Referable" $ "Thread " ++ show myself ++ " is watching " ++ toString k
        atomicModifyIORef' bRef $ watchRef' myself
          where watchRef' me (v, watches) =
                  ((v, Set.insert (me, context) watches), ())

    deadRef :: (Print k) => k -> VolatileRef k a
    deadRef name = VolatileRef (name, throw $ badRef "deadRef" name)

    getRefName :: VolatileRef k a -> k
    getRefName = fst . unwrapVolatileRef

badRef :: (Print k) => String -> k -> SomeException
badRef loc name = toException . HE.BadAccess $ loc ++ " <dead> #" ++ toString name

notify :: (MonadIO m) => (ByteString -> ReferableException) -> Watches -> m ()
notify evf = liftIO . mapM_ notify'
  where notify' (thread, context) = throwTo thread $ evf context
