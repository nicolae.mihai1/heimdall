{-# LANGUAGE OverloadedStrings #-}

module Data.Configurable where

import Control.Applicative       ((<|>))
import Control.Exception         (Exception)
import Control.Monad             (mapM)
import Control.Monad.Except      (runExceptT)
import Control.Monad.Failable    (Failable(..), hoist)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.ByteString.Char8     (ByteString, pack, unpack)
import Data.Char                 (isSpace)
import Data.IP                   (AddrRange,
                                  IPv4,
                                  IPv6,
                                  IP(..),
                                  fromHostAddress,
                                  fromHostAddress6,
                                  toHostAddress,
                                  toHostAddress6)
import Data.List                 (intercalate)
import Data.Maybe                (fromMaybe)
import Data.Typeable             (Typeable)
import Data.Word                 (Word16, Word32)
import Network.Socket
import Text.Read                 (readMaybe)

import qualified Data.ByteString.Char8 as B

data ConfigurableError = InvalidValue String
                       | InternalError String
                       deriving (Typeable, Show)

instance Exception ConfigurableError

class Configurable a where
    serialize   :: (Failable m) => a -> m ByteString
    deserialize :: (Failable m) => ByteString -> m a

instance Configurable Bool where
    serialize True  = return "true"
    serialize False = return "false"

    deserialize "true"  = return True
    deserialize "false" = return False
    deserialize str     = failure . InvalidValue $ "not a boolean: " ++ unpack str

instance Configurable Int where
    serialize   = return . pack . show
    deserialize = readNum

instance Configurable Word16 where
    serialize   = return . pack . show
    deserialize = readNum

instance Configurable Word32 where
    serialize   = return . pack . show
    deserialize = readNum

readNum :: (Num a, Read a, Monad m, Failable m) => ByteString -> m a
readNum str = check . readMaybe $ input
    where check Nothing  = failure . InvalidValue $ "not a number: " ++ input
          check (Just x) = return x
          input          = unpack str

instance Configurable a => Configurable [a] where
    serialize   = fmap (pack . intercalate ", " . fmap unpack) . mapM serialize
    deserialize = mapM deserialize . fmap trim . B.split ','
        where trim = snip . snip
              snip = B.reverse . B.dropWhile isSpace

instance {-# OVERLAPPING #-} Configurable String where
    serialize   = return . pack
    deserialize = return . unpack

instance (Configurable a, Configurable b) => Configurable (a, b) where
    serialize (a, b) = do
      x <- serialize a
      y <- serialize b
      return $ x <> " => " <> y
    deserialize str = do
      let (x, y0) = B.breakSubstring "=>" str
      y <- hoist (const $ InvalidValue $ unpack str) $ B.stripPrefix "=>" y0
      a <- deserialize x
      b <- deserialize y
      return (a, b)

instance Configurable ByteString where
    serialize   = return
    deserialize = return

instance Configurable SockAddr where
    serialize       = return . pack . show
    deserialize str = do
      mAddr4 <- tryParseSockAddr AF_INET input
      mAddr6 <- tryParseSockAddr AF_INET6 input
      check $ mAddr4 <|> mAddr6
          where input             = unpack str
                check Nothing     = failure . InvalidValue $ "not a network address: " ++ input
                check (Just addr) = return addr

instance Configurable IP where
    serialize       = return . pack . show
    deserialize str = do
      mAddr <- tryParseIP input
      check mAddr
          where input             = unpack str
                check Nothing     = failure . InvalidValue $ "not an IP address: " ++ input
                check (Just addr) = return addr

instance Configurable (AddrRange IPv4) where
    serialize   = return . pack . show
    deserialize = deserializeAddressRange

instance Configurable (AddrRange IPv6) where
    serialize   = return . pack . show
    deserialize = deserializeAddressRange

deserializeAddressRange :: (Read a, Failable m) => ByteString -> m a
deserializeAddressRange bytes = check . readMaybe $ str
        where str            = unpack bytes
              check Nothing  = failure . InvalidValue $ "not an address range: " ++ str
              check (Just x) = return x

deserializeOr :: (Failable m, Configurable a) => a -> ByteString -> m a
deserializeOr defValue str = do
  result <- runMaybeT . deserialize $ str
  return $ fromMaybe defValue result

addressFromSockAddr :: (Monad m, Failable m) => SockAddr -> m IP
addressFromSockAddr (SockAddrInet _ v4Host)      = return . IPv4 $ fromHostAddress v4Host
addressFromSockAddr (SockAddrInet6 _ _ v6Host _) = return . IPv6 $ fromHostAddress6 v6Host
addressFromSockAddr address = failure . InternalError $ "Unsupported address: " ++ show address

tryParseIP :: (Monad m) => String -> m (Maybe IP)
tryParseIP  = return . readMaybe

tryParseSockAddr :: (Monad m, Failable m) => Family -> String -> m (Maybe SockAddr)
tryParseSockAddr family@AF_INET =
    uncurry (tryParseSockAddr' family) . fmap (drop 1) . span (/= ':')
tryParseSockAddr family@AF_INET6 =
    uncurry (tryParseSockAddr' family) . fmap (drop 2) . span (/= ']')  . drop 1
tryParseSockAddr family =
    \str ->
      failure . InvalidValue $ "Unsupported address family " ++ show family ++ " parsing " ++ str

tryParseSockAddr' :: (Monad m) => Family -> String -> String -> m (Maybe SockAddr)
tryParseSockAddr' AF_INET addr port =
    runMaybeT $ do
      ip <- MaybeT . pure $ readMaybe addr
      portNum <- MaybeT . pure $ readMaybe port
      return $ SockAddrInet portNum (toHostAddress ip)
tryParseSockAddr' AF_INET6 addr port =
    runMaybeT $ do
      ip <- MaybeT . pure $ readMaybe addr
      portNum <- MaybeT . pure $ readMaybe port
      return $ SockAddrInet6 portNum 0 (toHostAddress6 ip) 0
tryParseSockAddr' _ _ _ = return Nothing

serializations :: (Monad m, Failable m, Bounded a, Configurable a, Enum a) => m [(a, ByteString)]
serializations = do
  result <- runExceptT $ mapM serialize values
  xs <- either internalError return result
  return $ zip values xs
      where values = [minBound..maxBound]
            internalError err = failure . InternalError $ "serializations: " ++ show err

type Serializations a = [(a, ByteString)]
