module System.Utils where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Int               (Int64)
import System.Clock           (Clock(Monotonic), TimeSpec(..), getTime)

timeSinceStart :: MonadIO m => m Int64
timeSinceStart = liftIO $
    getTime Monotonic >>= \(TimeSpec secs ns) ->
        return $ secs * 1000000000 + ns
