{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}
{- |
Description: Heimdall Redis Adapter
License: LGPL (See LICENSE)
Maintainer: erick@crankysurfer.com
Copyright: (c) Erick Gonzalez, 2018
-}

module Database.Adapter.Redis where

import Prelude         hiding   (lookup)
import Control.Monad            (void, when)
import Control.Monad.Trans      (lift)
import Control.Monad.IO.Class   (MonadIO, liftIO)
import Control.Monad.Except     (ExceptT(..), runExceptT, throwError)
import Control.Monad.Failable   (Failable(..), failableIO)
import Data.Binary              (Binary(..), encode, getWord8, putWord8)
import Data.ByteString.Char8    (ByteString, pack)
import Data.ByteString.Internal (c2w)
import Data.ByteString.Lazy     (toStrict)
import Data.Conduit             (yield)
import Data.IORef               (atomicModifyIORef', newIORef, readIORef)
import Data.Function            (on)
import Data.Hashes.FNV1a        (fnvOffsetBasis32, fnv1aHash32Base)
import Data.List                (sortBy)
import Data.Monoid              ((<>))
import Data.Time                (UTCTime, getCurrentTime)
import Data.Time.Calendar       (Day(..))
import Data.Time.Clock          (DiffTime,
                                 UTCTime(..),
                                 diffTimeToPicoseconds,
                                 picosecondsToDiffTime)
import Database.Adapter         (Adapter(..),
                                 Error(..),
                                 Event(..),
                                 KeyType(..),
                                 SubAction(..))

import qualified Data.ByteString.Char8 as B
import qualified Database.Redis as R
import qualified Data.Set as S

data Redis = Redis { getConnection :: R.Connection,
                     getWithMeta   :: Bool }

data ValueType = StringValue
               | SetValue
               | MapValue
               | NullValue
                 deriving (Read, Show, Eq, Enum)

instance Binary ValueType where
    put = putWord8 . fromIntegral . fromEnum
    get = getWord8 >>= return . toEnum . fromIntegral

instance Binary Day where
    put (ModifiedJulianDay x) =
        put x
    get =
        Data.Binary.get >>= return . ModifiedJulianDay

instance Binary DiffTime where
    put = put . diffTimeToPicoseconds
    get = Data.Binary.get >>= return . picosecondsToDiffTime

instance Binary UTCTime where
    put (UTCTime day diffTime) = do
      put day
      put diffTime
    get = do
      day <- Data.Binary.get
      diffTime <- Data.Binary.get
      return $ UTCTime day diffTime

type Hash = Integer
type Meta = (UTCTime, ValueType)

merkleTreeDepth :: Int
merkleTreeDepth = 18

maxHashTransactions :: Int
maxHashTransactions = 3

blockPrefix :: ByteString
blockPrefix = "__mblk_"

fromRedis :: (MonadIO m, Failable m) => m (Either R.Reply a) -> m a
fromRedis = (=<<) $ either (failure . fromReply) return

fromReply :: R.Reply -> Error
fromReply (R.Error message) = DatabaseError $ show message
fromReply r                 = UnexpectedResult $ show r

fromStatus :: (MonadIO m, Failable m) => R.Status -> m ()
fromStatus R.Ok   = return ()
fromStatus R.Pong = return ()
fromStatus (R.Status message) = failure $ Status message

redisOp :: (MonadIO m, Failable m) => Redis -> R.Redis (Either R.Reply a) -> m a
redisOp Redis {..} = fromRedis . failableIO . R.runRedis getConnection

keyIndex :: ByteString -> Int
keyIndex key = fromIntegral $ (hash key) `mod` (2 ^ merkleTreeDepth)
    where hash = notZero.foldl fnv1aHash32Base fnvOffsetBasis32 . asWordList
          notZero 0 = 1
          notZero r = r
          asWordList str = c2w . B.index str <$> [0..B.length str - 1]

update :: (Eq a) => a -> b -> [(a, b)] -> [(a,b)]
update k v xs = (k, v) : delete k xs

delete :: (Eq a) => a -> [(a, b)] -> [(a, b)]
delete k = filter ( (k /= ) . fst)

redisTransaction :: R.RedisTx (R.Queued a) -> ExceptT R.Reply R.Redis a
redisTransaction ops = do
  result <- lift $ R.multiExec ops
  case result of
    R.TxSuccess x -> return x
    R.TxAborted   -> throwError $ R.Error "Transaction aborted"
    R.TxError str -> throwError $ R.Error (pack str)

withMeta :: (MonadIO m, Failable m) => Redis -> ByteString -> ValueType -> R.RedisTx (R.Queued a) -> m a
withMeta adapter@Redis{..} key valueType actions = do
  redisOp adapter . runExceptT $
    if getWithMeta then do
       now <- liftIO getCurrentTime
       let meta    = (now, valueType)
           metaStr = toStrict . encode $ meta
       kvs0 <- ExceptT $ R.hgetall block
       let kvs  = update key metaStr kvs0
           hash = hashBlock kvs
       redisTransaction $ do
         void $ R.hset block key metaStr
         void $ R.set hName $ B.pack . show $ hash
         actions
    else
       redisTransaction actions
    where hName = "#" <> block
          ix           = keyIndex key
          block        = blockPrefix <> (pack . show $ ix)
          hashBlock    = hashBlock' . fmap (uncurry mappend) . sortBy (compare `on` fst)
          hashBlock'   = foldl hashFn fnvOffsetBasis32
          hashFn       = B.foldl hashFn'
          hashFn' base = fnv1aHash32Base base . c2w

instance Adapter Redis where
    new = do
      connection <- failableIO $ R.checkedConnect R.defaultConnectInfo
      return Redis { getConnection = connection,
                     getWithMeta   = True }

    get adapter key = do
      result <- lookup adapter key
      maybe (failure $ NotFound key) return result

    del adapter = mapM_ delOne
        where delOne key = withMeta adapter key NullValue $ R.del [key]

    lookup adapter  = redisOp adapter . R.get
    getMap adapter  = redisOp adapter . R.hgetall

    getMapKV adapter mapName key = do
      result <- lookupMap adapter mapName key
      maybe (failure $ NoSuchKey mapName key) return result

    setMapKVs adapter mapName = mapM_ setMapKV
        where setMapKV = withMeta adapter mapName MapValue . uncurry (R.hset mapName)

    delMapKVs adapter mapName = mapM_ delMapKV
        where delMapKV = withMeta adapter mapName NullValue . R.hdel mapName . pure

    lookupMap adapter mapName = redisOp adapter . R.hget mapName

    getSet adapter = redisOp adapter . R.smembers

    addToSet adapter setName = mapM_ addOne
        where addOne = withMeta adapter setName SetValue . R.sadd setName . pure

    delFromSet adapter setName values = do
      strs <- redisOp adapter $ R.smembers setName
      let strs' = S.toAscList $ foldl (flip S.delete) (S.fromList strs) values
      if null strs'
         then del adapter [setName]
         else mapM_ delOne values
        where delOne = withMeta adapter setName SetValue . R.srem setName . pure

    set adapter key value = do
      r <- withMeta adapter key StringValue $ R.set key value
      fromStatus r

    search adapter pat =
      searchFrom R.cursor0
          where searchFrom cursor = do
                  (next, r) <- lift . redisOp adapter $ R.scanOpts cursor R.defaultScanOpts {
                                      R.scanMatch = Just pat,
                                      R.scanCount = Just 1024 }
                  yield r
                  when (next /= R.cursor0) $ searchFrom next

    uponChange Redis{..} patterns0 handler =
      failableIO $ do
        patterns <- newIORef patterns0
        R.runRedis getConnection $ do
          void $ R.configSet "notify-keyspace-events" "KEA"
          R.pubSub (subscribe patterns0) $ \msg -> do
            let key = B.drop 1 $ B.dropWhile (/= ':') (R.msgChannel msg)
                event = case (R.msgMessage msg) of
                          "set"  -> Changed StringType key
                          "del"  -> Deleted StringType key
                          "hset" -> Changed MapType key
                          "hdel" -> Deleted MapType key
                          "sadd" -> Changed SetType key
                          "srem" -> Deleted SetType key
                          _      -> UnknownEvent (R.msgMessage msg)
            result <- handler event
            case result of
              Done -> readIORef patterns >>= return . unsubscribe
              Continue -> return mempty
              Unsubscribe patterns' ->
                  atomicModifyIORef' patterns $ \current ->
                    (updatePatternsWith S.delete patterns' current, unsubscribe patterns')
              Subscribe patterns' ->
                atomicModifyIORef' patterns $ \current ->
                    (updatePatternsWith S.insert patterns' current, subscribe patterns')

      where updatePatternsWith fun patterns current =
              S.toList $ foldl (flip fun) (S.fromList current) patterns
            subscription = mappend "__keyspace*__:"
            subscribe    = R.psubscribe . fmap subscription
            unsubscribe  = R.punsubscribe . fmap subscription
