{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Packet definition
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Heimdall.Packet where

import Control.Exception                (throw)
import Control.Monad                    ((<=<), void)
import Control.Monad.Failable           (Failable(..))
import Data.Configurable                (Configurable(..))
import Data.IP                          (IP(..),
                                         fromHostAddress,
                                         fromHostAddress6,
                                         toHostAddress,
                                         toHostAddress6)
import Data.Maybe                       (fromMaybe)
import Data.Print                       (Print(..))
import Text.Read                        (readMaybe)
import Data.Word                        (Word8, Word16, Word32)
import Foreign.Ptr                      (Ptr, castPtr, plusPtr)
import Foreign.C.Types                  (CChar)
import Foreign.Storable                 (Storable(..))
import Heimdall.Exceptions
import Network.Socket                   (Family(..))

import qualified Data.ByteString.Char8            as B
import qualified Data.Attoparsec.ByteString.Char8 as A

data Packet = Packet { getPktId            :: Word32,
                       getSrcIP            :: IP,
                       getDstIP            :: IP,
                       getFamily           :: Family,
                       getProto            :: Protocol,
                       getSrcPort          :: Word16,
                       getDstPort          :: Word16,
                       getTotalPayload     :: Word32,
                       getAvailablePayload :: Word32,
                       getPktLen           :: Word32,
                       getPktStart         :: Ptr CChar,
                       getPayload          :: Ptr CChar}
              deriving (Show, Eq)

-- For the love of Bob, do not touch this under penalty of a slow gruesome and painful
-- death!! This has to match the definition of packet_t in huvud.h and it is platform
-- specific bit fiddling. There are ways to make this safer but it would be slower, so
-- in the interest of warp speed, there is a lot of manual peek pokery here. So just.. don't.
--   _                   _
-- _( )                 ( )_
-- (_, |      __ __      | ,_)
--    \'\    /  ^  \    /'/
--     '\'\,/\      \,/'/'
--       '\| []   [] |/'
--         (_  /^\  _)
--           \  ~  /
--           /HHHHH\
--         /'/{^^^}\'\
--     _,/'/'  ^^^  '\'\,_
--    (_, |           | ,_)
--      (_)           (_)
instance Storable Packet where
    alignment    = const 8
    sizeOf       = const 64
    poke pktPtr Packet{..} = do
      poke   ptr                  getPktId
      poke   (plusPtr ptr 0x24) $ addressLengthFor getFamily
      pokeIP (plusPtr ptr 0x4)    getSrcIP
      pokeIP (plusPtr ptr 0x14)   getDstIP
      poke   (plusPtr ptr 0x25) $ addressFamilyCode getFamily
      poke   (plusPtr ptr 0x26) $ fromEnum getProto
      poke   (plusPtr ptr 0x28)   getSrcPort
      poke   (plusPtr ptr 0x2A)   getDstPort
      poke   (plusPtr ptr 0x2C)   getTotalPayload
      poke   (plusPtr ptr 0x30)   getAvailablePayload
      poke   (plusPtr ptr 0x34)   getPktLen
      poke   (plusPtr ptr 0x38)   getPktStart
      poke   (plusPtr ptr 0x40)   getPayload
          where ptr = castPtr pktPtr

    peek pktPtr = do
      packetId         <- peek $ ptr
      addressLen       <- peek $ plusPtr ptr 0x24
      srcIP            <- peekIP addressLen $ plusPtr ptr 0x4
      dstIP            <- peekIP addressLen $ plusPtr ptr 0x14
      family           <- (peek $ plusPtr ptr 0x25) >>= return.addressFamilyFrom
      proto            <- (peek $ plusPtr ptr 0x26) >>=
                           (return.toEnum.fromIntegral :: Word8 -> IO Protocol)
      srcPort          <- peek $ plusPtr ptr 0x28
      dstPort          <- peek $ plusPtr ptr 0x2A
      totalPayload     <- peek $ plusPtr ptr 0x2C
      availablePayload <- peek $ plusPtr ptr 0x30
      pktLen           <- peek $ plusPtr ptr 0x34
      pktStart         <- peek $ plusPtr ptr 0x38
      payload          <- peek $ plusPtr ptr 0x40
      return Packet { getPktId            = packetId,
                      getSrcIP            = srcIP,
                      getDstIP            = dstIP,
                      getFamily           = family,
                      getProto            = proto,
                      getSrcPort          = srcPort,
                      getDstPort          = dstPort,
                      getTotalPayload     = totalPayload,
                      getAvailablePayload = availablePayload,
                      getPktLen           = pktLen,
                      getPktStart         = pktStart,
                      getPayload          = payload }
          where ptr = castPtr pktPtr

data Protocol = IP_Protocol
              | IP_Raw_Protocol
              | GRE_Protocol
              | UDP_Protocol
              | TCP_Protocol
              | ICMP_Protocol
              | ICMPv6_Protocol
              | ProtocolNumber Word8 deriving Eq

instance Show Protocol where
    showsPrec _ IP_Protocol     = ("ip" ++)
    showsPrec _ IP_Raw_Protocol = ("raw" ++)
    showsPrec _ GRE_Protocol    = ("gre" ++)
    showsPrec _ UDP_Protocol    = ("udp" ++)
    showsPrec _ TCP_Protocol    = ("tcp" ++)
    showsPrec _ ICMP_Protocol   = ("icmp" ++)
    showsPrec _ ICMPv6_Protocol  = ("icmpv6" ++)
    showsPrec _ (ProtocolNumber x) = (("protocol " ++ show x) ++)

instance Print Protocol where
    toString = show

instance Enum Protocol where
    toEnum 0     = IP_Protocol
    toEnum 1     = ICMP_Protocol
    toEnum 6     = TCP_Protocol
    toEnum 17    = UDP_Protocol
    toEnum 47    = GRE_Protocol
    toEnum 58    = ICMPv6_Protocol
    toEnum 255   = IP_Raw_Protocol -- this is probably Linux specific. Sorry
    toEnum p =
      fromMaybe (throw $ UnknownProtocol p) $ customProtocol p

    fromEnum IP_Protocol        = 0
    fromEnum GRE_Protocol       = 47
    fromEnum UDP_Protocol       = 17
    fromEnum TCP_Protocol       = 6
    fromEnum ICMP_Protocol      = 17
    fromEnum ICMPv6_Protocol    = 6
    fromEnum IP_Raw_Protocol    = 255
    fromEnum (ProtocolNumber x) = fromIntegral x

instance Configurable Protocol where
    serialize IP_Protocol        = return "ip"
    serialize TCP_Protocol       = return "tcp"
    serialize UDP_Protocol       = return "udp"
    serialize GRE_Protocol       = return "gre"
    serialize IP_Raw_Protocol    = return "raw"
    serialize ICMP_Protocol      = return "icmp"
    serialize ICMPv6_Protocol    = return "icmpv6"
    serialize (ProtocolNumber x) = return $ B.pack $ "protocol " ++ show x
    deserialize str = maybeFail $ A.parseOnly protocolParser str
      where maybeFail     = maybeBadProto <=< maybeInvalid
            maybeInvalid  = either (failure . InvalidValue) return
            maybeBadProto = either failure return

protocolParser :: A.Parser (Either Exception Protocol)
protocolParser = A.choice [ip, tcp, udp, gre, raw, icmp, icmpv6, protocolNumber]
  where ip     = A.string "ip"  >> return (Right IP_Protocol)
        tcp    = A.string "tcp" >> return (Right TCP_Protocol)
        udp    = A.string "udp" >> return (Right UDP_Protocol)
        gre    = A.string "gre" >> return (Right GRE_Protocol)
        raw    = A.string "raw" >> return (Right IP_Raw_Protocol)
        icmp   = A.string "icmp"   >> return (Right ICMP_Protocol)
        icmpv6 = A.string "icmpv6" >> return (Right ICMPv6_Protocol)
        protocolNumber = do
          void $ A.string "protocol"
          void $ A.takeWhile1 A.isSpace
          maybeUnknown <$> A.many1 A.digit
        maybeUnknown p =
          let proto = customProtocol =<< readMaybe p
          in maybe (Left $ unknownProto p) Right proto
        unknownProto p =
          InvalidProtocol $ "unknown protocol: " ++ show p

data EtherType = EtherIPv4
               | EtherIPv6
                 deriving (Eq, Show)

instance Enum EtherType where
    toEnum 0x0800 = EtherIPv4
    toEnum 0x86dd = EtherIPv6
    toEnum p     = throw $ UnknownProtocol p
    fromEnum EtherIPv4 = 0x0800
    fromEnum EtherIPv6 = 0x86dd

customProtocol :: Int -> Maybe Protocol
customProtocol x
  | x < 255   = return $ ProtocolNumber $ fromIntegral x
  | otherwise = Nothing

etherTypeFor :: Family -> EtherType
etherTypeFor AF_INET  = EtherIPv4
etherTypeFor AF_INET6 = EtherIPv6
etherTypeFor af       = throw $ UnsupportedAddressFamily af

addressFamilyCode :: Family -> Word8
addressFamilyCode AF_INET  = 2
addressFamilyCode AF_INET6 = 30
addressFamilyCode family   = throw $ UnsupportedAddressFamily family

addressFamilyFrom :: Word8 -> Family
addressFamilyFrom 2  = AF_INET
addressFamilyFrom 30 = AF_INET6
addressFamilyFrom af = throw $ UnknownAddressFamilyCode af

addressLengthFor :: Family -> Word8
addressLengthFor AF_INET  = 1
addressLengthFor AF_INET6 = 4
addressLengthFor af       = throw $ UnsupportedAddressFamily af

peekIP :: Word8 -> Ptr Word32 -> IO IP
peekIP 1 ptr =
    peek ptr >>= (return . IPv4 . fromHostAddress)
peekIP 4 ptr = do
    w1 <- peek ptr
    w2 <- peek $ plusPtr ptr 0x4
    w3 <- peek $ plusPtr ptr 0x8
    w4 <- peek $ plusPtr ptr 0xC
    return . IPv6 . fromHostAddress6 $ (w1, w2, w3, w4)
peekIP n _ =
    throw $ InvalidIPAddressLength n

pokeIP :: Ptr Word32 -> IP -> IO ()
pokeIP ptr (IPv4 ip4) =
    poke ptr $ toHostAddress ip4
pokeIP ptr (IPv6 ip6) =
    let (w1, w2, w3, w4) = toHostAddress6 ip6
    in do
      poke ptr               w1
      poke (plusPtr ptr 0x4) w2
      poke (plusPtr ptr 0x8) w3
      poke (plusPtr ptr 0xC) w4
