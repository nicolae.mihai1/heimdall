{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Heimdall.Types.Network where

import Data.ByteString               (ByteString)
import Control.Lens                  (makeLenses)
import Control.Concurrent.Async      (Async)
import Control.Concurrent.MVar       (MVar)
import Data.Default                  (Default, def)
import Data.Word                     (Word32)
import Network.Socket                (HostName, PortNumber)

import qualified Network.Connection               as NC

type VRFID = Word32

vrfKeyPrefix :: ByteString
vrfKeyPrefix = "config.heimdall.vrf."

data NetworkConnection = NetworkConnection { _connHostName :: HostName,
                                             _connPortNum  :: PortNumber,
                                             _networkConn  :: MVar NC.Connection,
                                             _connContext  :: NC.ConnectionContext,
                                             _controlProc  :: Maybe (Async ()),
                                             _connSettings :: Settings }

data Settings = Settings { useTLS      :: Bool,
                           autoConnect :: Bool }

instance Default Settings where
    def = Settings { useTLS      = False,
                     autoConnect = True }

makeLenses ''NetworkConnection
makeLenses ''Settings