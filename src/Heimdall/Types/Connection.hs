{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Types.Connection where

import Data.ByteString.Char8         (ByteString, pack)
import Data.Configurable             (Configurable(..), readNum)
import Data.Int                      (Int32)
import Data.Print                    (Print, toString)

type ConnectionTag = Int32

connTrackKeyPrefix :: ByteString
connTrackKeyPrefix = "config.heimdall.conntrack."

instance Configurable ConnectionTag where
  serialize   = return . pack . show
  deserialize = readNum

instance Print ConnectionTag where
  toString = show
