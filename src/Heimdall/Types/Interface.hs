{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.Interface where

import Control.Applicative           ((<|>))
import Data.Bifunctor                (first)
import Data.ByteString.Char8         (ByteString, spanEnd, pack, unpack)
import Data.Configurable             (Configurable(..))
import Data.Maybe                    (isJust, fromJust)
import Data.Referable                (Referable, VolatileRef)
import Data.Print                    (Print, toString)
import Data.Set                      (Set)
import Control.Concurrent.Async      (Async)
import Control.Monad                 (guard, void)
import Control.Monad.Failable        (failure)
import Control.Lens                  (makeLenses)
import Text.Read                     (readMaybe)
import Heimdall.Exceptions
import Heimdall.Types.Connection
import Heimdall.Types.Network
import Heimdall.Packet

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.Set as Set

newtype IntfName = IntfName { unwrapIntfName :: (IntfType, Int) }
    deriving (Eq, Ord)

data IntfType = TunnelIntf
              | QueueIntf
              | VIFIntf
                deriving (Eq, Ord, Enum, Bounded)

instance Show IntfType where
    showsPrec _ TunnelIntf = ("tunnel" ++)
    showsPrec _ QueueIntf  = ("queue" ++)
    showsPrec _ VIFIntf = ("vif" ++)

instance Read IntfType where
    readsPrec _ "queue"  = [(QueueIntf, "")]
    readsPrec _ "vif"    = [(VIFIntf, "")]
    readsPrec _ "tunnel" = [(TunnelIntf, "")]
    readsPrec _ _        = []

instance Print IntfName where
    toString = show

intfKeyPrefix :: ByteString
intfKeyPrefix = "config.heimdall."

getIntfType :: IntfName -> IntfType
getIntfType = fst . unwrapIntfName

getIntfID :: IntfName -> Int
getIntfID = snd . unwrapIntfName

instance Show IntfName where
    showsPrec _ (IntfName (t, iD)) = ((show t ++ '/':show iD) ++)

instance Configurable IntfName where
  serialize   = return . pack . show
  deserialize = either (failure . InvalidValue) return . A.parseOnly parseIntfName

parseIntfName :: A.Parser IntfName
parseIntfName = do
  intfType <- readMaybe.unpack <$> A.choice (fmap A.string allTypes)
  guard (isJust intfType)
  void $ A.char '/'
  iD <- readMaybe <$> A.many1 (A.satisfy A.isDigit)
  guard (isJust iD)
  return $ IntfName (fromJust intfType, fromJust iD)
      where allTypes = pack.show <$> ([minBound..maxBound] :: [IntfType])

getIntfNameFromKey :: ByteString -> Maybe IntfName
getIntfNameFromKey = check . first getType . fmap getID . splitDot
    where splitDot = spanEnd (/= '.')
          getID    = readMaybe . unpack
          getType  = readMaybe . unpack . snd . splitDot . fst . (spanEnd (== '.'))
          check (Just intfType, Just iD) = Just $ IntfName (intfType, iD)
          check _                        = Nothing

getIntfRootKey :: ByteString -> Maybe ByteString
getIntfRootKey ""  = Nothing
getIntfRootKey key =
    case getIntfNameFromKey key of
      Just _ ->
        return key
      Nothing ->
        getIntfRootKey . fst . spanEnd (== '.') . fst $ spanEnd (/= '.') key

data Interface_ acl policyMap = Interface { _intfName         :: IntfName,
                                            _intfVRFId        :: VRFID,
                                            _intfPolicyMap    :: policyMap,
                                            _intfCapabilities :: Capabilities,
                                            _intfConnTag      :: Maybe ConnectionTag,
                                            _intfMasquerading :: Bool,
                                            _intfRPIDB        :: Maybe (IDB_ acl policyMap),
                                            _intfLogACL       :: Maybe acl,
                                            _intfForwarder    :: Forwarder_ acl policyMap }

data ICB_ acl policyMap = ICB { _icbIntf   :: Maybe (Interface_ acl policyMap),
                                _icbThread :: Async (),
                                _icbInfo   :: [(ByteString,ByteString)] }

instance Referable (ICB_ acl policyMap)

type IDB_ acl policyMap = VolatileRef IntfName (ICB_ acl policyMap)

type Forwarder_ acl policyMap = Packet -> Interface_ acl policyMap -> IO ()

data XmitDirection = Rx | Tx deriving Show

data Capability = RxData
                | TxData
                  deriving (Eq, Ord)

can :: Capability -> Capabilities
can = Capabilities . Set.singleton

(<?>) :: Interface_ acl policyMap -> Capabilities -> Bool
(<?>) Interface{..} (Capabilities setA) = canAllOf _intfCapabilities
    where canAllOf (Capabilities setB) = setA `Set.isSubsetOf` setB

newtype Capabilities = Capabilities (Set Capability) deriving (Semigroup, Monoid)

data ServiceType = IngressService
                 | EgressService
                 | ForwardingService
                   deriving (Bounded, Enum)

instance Configurable ServiceType where
    serialize IngressService    = return "ingress"
    serialize EgressService     = return "egress"
    serialize ForwardingService = return "forwarding"
    deserialize = either (failure . InvalidValue) return . A.parseOnly serviceTypeParser
        where serviceTypeParser = ingressServiceType <|>
                                  egressServiceType <|>
                                  forwardingServiceType

ingressServiceType :: A.Parser ServiceType
ingressServiceType = do
  void $ A.string "ingress"
  return IngressService

egressServiceType :: A.Parser ServiceType
egressServiceType = do
  void $ A.string "egress"
  return EgressService

forwardingServiceType :: A.Parser ServiceType
forwardingServiceType = do
  void $ A.string "forwarding"
  return ForwardingService

makeLenses ''Interface_
makeLenses ''ICB_
