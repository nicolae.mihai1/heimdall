{-# LANGUAGE OverloadedStrings, UndecidableInstances #-}
{- |
Description: Heimdall Access Control Lists
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018k
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Common.ACL where

import Control.Applicative              ((<|>))
import Control.Lens                     ((.=))
import Control.Monad                    (void, when)
import Control.Monad.Except             (ExceptT(..), runExceptT, throwError)
import Control.Monad.Failable           (Failable(..))
import Control.Monad.Writer             (execWriter, tell)
import Control.Monad.Trans              (lift)
import Control.Monad.Trans.Maybe        (MaybeT(..), runMaybeT)
import Control.Monad.Trans.State.Strict (StateT, execStateT)
import Data.Bitraversable               (bimapM)
import Data.ByteString.Char8            (ByteString, unpack, isPrefixOf)
import Data.Default                     (def)
import Data.Maybe                       (fromJust)
import Data.Monoid                      ((<>))
import Data.Print                       (Print(..))
import Heimdall.Exceptions
import Heimdall.Network.Utils           (addressFamily)
import Heimdall.Packet                  (protocolParser)
import Heimdall.Types.ACL
import Heimdall.Types.Flow
import Network.Socket                   (Family(..))
import Text.Read                        (readMaybe)

import qualified Data.Attoparsec.ByteString.Char8 as A

instance Show ACE where
    showsPrec _ ACE {..} =
      let str = execWriter $ do
                    tell $ show _aceAccess
                    perhaps ""     _aceProtocol
                    perhaps "dir"  _aceDirection
                    perhaps "port" _acePorts
                    perhaps "src"  _aceSrcRange
                    perhaps "dst"  _aceDstRange
                    perhaps "dev"  _aceDevice
                    perhaps "conn" _aceConnTag
      in (str ++)
        where perhaps _ Nothing  = return mempty
              perhaps p (Just x) = tell $ ws p ++ ' ':toString x
              ws "" = ""
              ws p  = ' ':p

instance Show Access where
    showsPrec _ Permit = ("permit" ++)
    showsPrec _ Deny   = ("deny" ++)

instance Show PortRange where
    showsPrec p (PortRange (start, end))
        | start == end = showsPrec p start
        | otherwise    = (showsPrec p start) . (':':) . (showsPrec p end)

instance Print PortRange where
    toString = show

instance Show IPRange where
    showsPrec p (IPRange (start, end))
        | start == end = showsPrec p start
        | otherwise    = showsPrec p start . ('-':) . showsPrec p end

instance Print IPRange where
    toString = show

type Help = [(String, String)]

type ParserT a = StateT a (ExceptT Help A.Parser)

liftP :: A.Parser b -> ParserT a b
liftP = lift . lift

liftE :: ExceptT Help A.Parser b -> ParserT a b
liftE = lift

parseACE :: (Monad m, Failable m) => ByteString -> m (Either Help ACE)
parseACE = either failed return . A.parseOnly (runExceptT $ execStateT aceParser def)
  where failed = failure . InvalidValue

help :: Help -> ParserT a ()
help hints = do
  helpRequest <- liftP $ A.skipSpace >> A.peekChar
  case helpRequest of
    Just '?' ->
      emitHelp
    _ ->
      return ()
    where emitHelp = liftE . throwError $ hints

aceParser :: ParserT ACE ()
aceParser = do
  help [("permit", "Access is permitted to matching entry"),
        ("deny", "Deny access to matching entry")]
  access <- lift . ExceptT $ permit <|> deny <|> fail "Expected <permit | deny>"
  aceAccess .= fromJust access
  aceFilter

keyword :: ByteString -> a -> A.Parser (Either Help (Maybe a))
keyword word result = do
  A.skipSpace
  matchWord <|> do
    str <- A.takeWhile1 (not . A.isSpace)
    if str `isPrefixOf` word
      then return $ Left [(unpack word, "")]
      else fail $ "expected: " ++ unpack word ++ " got " ++ unpack str
    where matchWord = do
                  void $ A.string word
                  return . pure $ Just result

permit :: A.Parser (Either Help (Maybe Access))
permit = keyword "permit" Permit

deny :: A.Parser (Either Help (Maybe Access))
deny = keyword "deny" Deny

aceFilter :: ParserT ACE ()
aceFilter = do
  noMoreFilters <- liftP A.atEnd
  if noMoreFilters
     then return ()
     else do
       void . liftP $ A.space >> A.skipSpace
       noMoreFilters' <- liftP A.atEnd
       if noMoreFilters'
         then return ()
         else do
           parseProtocol
           hasJunk <- not <$> liftP A.atEnd
           when hasJunk $ do
             junk <- unpack <$> liftP A.takeByteString
             fail $ "Unexpected input: " ++ junk

protocolHelp :: Help
protocolHelp = [("ip", "Match IP packets"),
                ("tcp", "L4 protocol is TCP"),
                ("udp", "L4 protocol is UDP")]

dirHelp :: Help
dirHelp = [("dir", "<upstream | downstream>")]

portsHelp :: Help
portsHelp = [("port", "TCP or UDP port number (i.e. <num> | <start:end> | <start-end>)")]

srcIPHelp :: Help
srcIPHelp = [("src", "Source CIDR IP address")]

dstIPHelp :: Help
dstIPHelp = [("dst", "Destination CIDR IP address")]

devHelp :: Help
devHelp =  [("dev", "Name of hardware device")]

connTagHelp :: Help
connTagHelp = [("conn", "Connection tag value")]

parseProtocol :: ParserT ACE ()
parseProtocol = do
  help $ protocolHelp <> dirHelp <> portsHelp <> srcIPHelp <> dstIPHelp <> devHelp <> connTagHelp
  proto <- liftP protocolParser
  aceProtocol .= toMaybe proto
  parseDir
  where toMaybe = either (const Nothing) Just

parseDir :: ParserT ACE ()
parseDir = do
  help $ dirHelp <> portsHelp <> srcIPHelp <> dstIPHelp <> devHelp <> connTagHelp
  void . runMaybeT $ do
    MaybeT . lift . ExceptT $ keyword "dir" () <|> return (pure Nothing)
    lift $ do
      help [("upstream", "Upstream traffic (from initiator)"),
            ("downstream", "Downstream traffic (to initiator)")]
      dir <- lift . ExceptT $ keyword "upstream" Upstream <|> keyword "downstream" Downstream
      aceDirection .= dir
  parsePorts

parsePorts :: ParserT ACE ()
parsePorts = do
  help $ portsHelp <> srcIPHelp <> dstIPHelp <> devHelp <> connTagHelp
  void . runMaybeT $ do
    MaybeT . lift . ExceptT $ keyword "port" () <|> keyword "ports" () <|> return (pure Nothing)
    lift $ do
      help [("", "<num | start:end> (0 < num < 65536)")]
      startPort' <- liftP $ (A.decimal A.<?> "invalid port number")
      checkRange startPort'
      nextChar <- liftP A.peekChar
      let done      = acePorts .= Just (PortRange (startPort, startPort))
          startPort = fromIntegral startPort'
      case nextChar of
        Just ' ' ->
          done
        Just _ -> do
          liftP . void $ A.char ':' <|> A.char '-'
          endPort <- liftP (A.decimal A.<?> "invalid port number")
          checkRange endPort
          acePorts .= Just (PortRange (startPort, fromIntegral endPort))
        Nothing ->
          done
  parseSrcIP
    where checkRange port =
              when ( port > (65535 :: Int)) $
                   fail $ "port " ++ show port ++ " out of range"

parseSrcIP :: ParserT ACE ()
parseSrcIP = do
  help $ srcIPHelp <> dstIPHelp <> devHelp <> connTagHelp
  void . runMaybeT $ do
    range <- parseIPFilter "src"
    aceSrcRange .= Just range
  parseDstIP

parseDstIP :: ParserT ACE ()
parseDstIP = do
  help $ dstIPHelp <> devHelp <> connTagHelp
  void . runMaybeT $ do
    range <- parseIPFilter "dst"
    aceDstRange .= Just range
  parseDevice

parseDevice :: ParserT ACE ()
parseDevice = do
  help $ devHelp <> connTagHelp
  void . runMaybeT $ do
    MaybeT . lift . ExceptT $ keyword "dev" () <|> keyword "device" () <|> return (pure Nothing)
    lift . liftP $ A.skipSpace
    lift . help $ [("", "<device name>")]
    dev <- MaybeT . liftP $ parseDevName
    aceDevice .= Just dev
  parseConnTag
        where parseDevName = unpack <$> A.takeWhile1 (not . A.isSpace) >>= return . Just

parseConnTag :: ParserT ACE ()
parseConnTag = do
  help connTagHelp
  void . runMaybeT $ do
    MaybeT . lift . ExceptT $ keyword "conn" () <|> keyword "connection" () <|> return (pure Nothing)
    lift . liftP $ A.skipSpace
    lift . help $ [("", "<connection tag>")]
    tag <- MaybeT $ parseTag >>= return . Just
    aceConnTag .= Just tag
        where parseTag = do
                help [("", "<Connection tag value>")]
                liftP $ A.decimal <|> (A.string "0x" >> A.hexadecimal)

parseIPRange :: ParserT ACE IPRange
parseIPRange = do
  help [("", "<from IP address>[-<to IP address>]")]
  (input,range) <- liftP (parseIPStr A.<?> "Missing IP address")
  case bimapM readMaybe readMaybe range of
    Nothing    ->
      fail $ "Bad IP address: " ++ unpack input
    Just ipRange@(start,end) | start > end ->
      fail $ "End of range must be greater than start: " ++ show (IPRange ipRange)
    Just ipRange ->
      return $ IPRange ipRange
    where parseIPStr = A.match $ do
            start <- A.takeWhile1 (not . isEndOfIp)
            end   <- A.option start $
                       A.char '-' >> A.takeWhile1 (not . isEndOfIp)
            return $ (unpack start,unpack end)
          isEndOfIp c = A.isSpace c || c == '-'

parseIPFilter :: ByteString -> MaybeT (ParserT ACE) IPRange
parseIPFilter str = do
  MaybeT . lift . ExceptT  $ keyword str () <|> return (pure Nothing)
  lift $ do
    liftP $ A.skipSpace
    parseIPRange

aceFamily :: ACE -> Maybe Family
aceFamily ACE {..} =
  let getFamily (IPRange (start,_)) = addressFamily start
      srcFamily  = maybe dstFamily getFamily _aceSrcRange
      dstFamily  = maybe srcFamily getFamily _aceDstRange
  in _aceSrcRange <|> _aceDstRange >>
        if srcFamily == dstFamily
          then return srcFamily
          else Nothing
